﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequentialJumper : MonoBehaviour
{
    public IEnumerator JumpOverTime(Vector2 start, Vector2 end)
    {
        bool jumpIsCompleted = false;
        start = transform.position;
        float t = 0.0f;

        while (!jumpIsCompleted)
        {
            transform.localPosition = Vector2.Lerp(start, end, t);
            t += Time.deltaTime;

            if (t >= 1)
            {
                jumpIsCompleted = true;
            }
            yield return null;
        }
    }
}
