﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetweenCharactersLocationFinder : MonoBehaviour
{
    public CutsceneSequencer endCutsceneSequencer;
    public Transform player;
    public Transform natalie;
    public Transform afterCutsceneTransform;
    public float afterCutsceneTransformPositionRight = 100;

    private void OnEnable()
    {
        endCutsceneSequencer.OnCutsceneStart += SetLocation;
    }

    private void OnDisable()
    {
        endCutsceneSequencer.OnCutsceneStart -= SetLocation;
    }

    public void SetLocation()
    {
        transform.position = (player.position + natalie.position) / 2;
        afterCutsceneTransform.position = new Vector3(transform.position.x + afterCutsceneTransformPositionRight, transform.position.y, transform.position.z);
    }
}
