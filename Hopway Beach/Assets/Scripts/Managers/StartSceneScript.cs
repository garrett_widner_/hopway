﻿using UnityEngine;

public class StartSceneScript : MonoBehaviour
{
    public ScenePassageway passageToFirstScene;

    public void Start()
    {
        passageToFirstScene.UsePassage();
    }



}
