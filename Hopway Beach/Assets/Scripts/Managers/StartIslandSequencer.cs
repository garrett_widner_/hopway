﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartIslandSequencer : MonoBehaviour
{
    public StatusBar foodBar;
    public int treesChoppedAfter;
    public int raftBuiltAfter;

    [Header("Player Location")]
    public Transform playerTransform;
    public Transform rightMost;

    [Header("Tree/Stump")]
    public GameObject[] stumps;
    public GameObject[] trees;
    public GameObject[] logs;

    [Header("Raft")]
    public GameObject raft;

    [Header("Natalie")]
    public Transform natalie;
    public Animator natAnimator;
    public string animatorTrigger;
    public Transform collectingLocation;
    public Transform postChopLocation;
    public Transform raftBuildingLocation;

    private bool hasStartedCollecting = false;
    private bool hasChoppedTrees = false;
    private bool hasBuiltRaft = false;

    public int CurrentBuildingStage
    {
        get
        {
            if (hasBuiltRaft)
                return 4;
            else if (hasChoppedTrees)
                return 3;
            else if (hasStartedCollecting)
                return 2;
            else
                return 1;
        }
    }

    private bool CanPlayerSeeIsland
    {
        get
        {
            if(playerTransform.position.x > rightMost.position.x)
            {
                return false;
            }
            return true;
        }
    }

    private void OnEnable()
    {
        foodBar.OnStatLevelIncreasedImmediate += CheckForStateChange;
    }

    private void OnDisable()
    {
        foodBar.OnStatLevelIncreasedImmediate += CheckForStateChange;

    }

    private void Update()
    {
        if(!hasStartedCollecting)
        {
            if(!CanPlayerSeeIsland)
            {
                StartCollecting();
            }
        }

        /*
        if(Input.GetKeyDown(KeyCode.Y))
        {
            StartCollecting();
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            ChopTreesDown();
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            BuildRaft();
        }
        */
    }

    private void CheckForStateChange(float increase)
    {
        if(!hasChoppedTrees)
        {
            if(foodBar.CurrentStatLevel >= treesChoppedAfter)
            {
                if(!CanPlayerSeeIsland)
                {
                    //print("Chopped");
                    ChopTreesDown();
                }
            }
        }
        if(!hasBuiltRaft)
        {
            if(foodBar.CurrentStatLevel >= raftBuiltAfter)
            {
                if(!CanPlayerSeeIsland)
                {
                    //print("Built");
                    BuildRaft();
                }
            }
        }
    }

    private void StartCollecting()
    {
        hasStartedCollecting = true;
        natAnimator.SetTrigger(animatorTrigger);
        natalie.position = collectingLocation.position;
    }

    private void ChopTreesDown()
    {
        hasChoppedTrees = true;
        natalie.position = postChopLocation.position;
        foreach(GameObject log in logs)
        {
            log.SetActive(true);
        }
        foreach(GameObject stump in stumps)
        {
            stump.SetActive(true);
        }
        foreach(GameObject tree in trees)
        {
            tree.SetActive(false);
        }
    }

    private void BuildRaft()
    {
        hasBuiltRaft = true;
        natalie.position = raftBuildingLocation.position;
        foreach (GameObject log in logs)
        {
            log.SetActive(false);
        }
        raft.SetActive(true);
    }


}
