﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusBarLengthSetter : StatusBar
{
    protected override void Start()
    {
        float totalPickupsToFind = 0f;
        List<StatPickup> allPickups = new List<StatPickup>();
        allPickups = new List<StatPickup>(FindObjectsOfType<StatPickup>());
        foreach (StatPickup pickup in allPickups)
        {
            totalPickupsToFind += pickup.statIncrease;
        }
        //print(totalPickupsToFind);
        maxStatLevel = totalPickupsToFind;
        timedHealingStopsWhenReachingExtremes = false;
        SetCurrentStatLevelImmediate(startingStatLevel);
        RunBarDimensionsSetup();
    }

}
