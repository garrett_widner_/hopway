﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueLocationPlacer : MonoBehaviour
{
    public DialogueManager dialogueManager;
    public BoxLocator[] boxLocations;
    public Transform textBox;
    public Camera mainCam;

    public void PlaceBox()
    {
        if(dialogueManager.StatementsQueue.Count >= 1)
        {
            string characterName = dialogueManager.StatementsQueue.Peek().characterName;
            foreach (BoxLocator locator in boxLocations)
            {
                if (locator.name.ToLower() == characterName.ToLower())
                {
                    textBox.transform.position = mainCam.WorldToScreenPoint(locator.boxLocation.position);
                }
            }
        }
        
    }

    [System.Serializable]
    public struct BoxLocator
    {
        public string name;
        public Transform boxLocation;
    }

}
