﻿using UnityEngine;

public class ScenePassageway : MonoBehaviour
{
    public int ID;
    public int connectedID;
    public string connectedScene;
    public bool isEntrance;
    public bool isExit;

    public delegate void PassageUseAction(ScenePassageway passage);
    public event PassageUseAction OnPassageUsed;

    public void UsePassage()
    {
        if (OnPassageUsed != null)
        {
            OnPassageUsed(this);
        }
    }
}