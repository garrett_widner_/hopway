﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuControlChanger : MonoBehaviour
{
    private PlayerActions playerActions;
    public GameObject controllerButtons;
    public GameObject keyboardButtons;

    private bool isCurrentlyUsingController;

    private void Start()
    {
        playerActions = PlayerActions.CreateWithDefaultBindings();
    }

    private void Update()
    {
        if(playerActions.LastInputType == InControl.BindingSourceType.DeviceBindingSource && !isCurrentlyUsingController)
        {
            isCurrentlyUsingController = true;
            controllerButtons.SetActive(true);
            keyboardButtons.SetActive(false);
        }
        else if(playerActions.LastInputType == InControl.BindingSourceType.KeyBindingSource && isCurrentlyUsingController)
        {
            isCurrentlyUsingController = false;
            controllerButtons.SetActive(false);
            keyboardButtons.SetActive(true);
        }
    }
}
