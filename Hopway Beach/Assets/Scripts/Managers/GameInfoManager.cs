﻿using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class GameInfoManager : Singleton
{
    public GameState gameStateInfo;

    public PlayerController player;

    public Canvas HUDCanvas;

    public Camera cam;

    private int idForEntrancePassageFromLastScene = 1;
    private ScenePassageway entrancePassageFromLastScene;
    private string currentSceneName = "";
    private bool isEmptyStartupScene = true;

    private List<ScenePassageway> scenePassageways = new List<ScenePassageway>();

    public delegate void RoomChangedAction();
    public event RoomChangedAction OnRoomChanged;

    protected override void Awake()
    {
        base.Awake();
    }

    private void OnEnable()
    {
        OnNewSceneLoad();
    }

    private void OnDisable()
    {
        OnOldSceneUnload();
    }

    private void Start()
    {

    }

    private void Update()
    {
        if(currentSceneName != SceneManager.GetActiveScene().name)
        {
            OnNewSceneLoad();
        }
    }

    private void TransitionBetweenScenes(ScenePassageway passage)
    {
        idForEntrancePassageFromLastScene = passage.connectedID;

        OnOldSceneUnload();

        SceneManager.LoadScene(passage.connectedScene);
    }

    private void OnNewSceneLoad()
    {
        currentSceneName = SceneManager.GetActiveScene().name;

        SetUpPassageways();

        SetUpPlayer();

        SetUpHUD();

        CollectSceneInfo();

        if (isEmptyStartupScene)
        {
            SetUpStartupScene();
        }
    }

    private void SetUpPassageways()
    {
        scenePassageways = new List<ScenePassageway>(FindObjectsOfType<ScenePassageway>());
        entrancePassageFromLastScene = null;

        bool alreadyFoundEntrance = false;
        foreach (ScenePassageway passage in scenePassageways)
        {
            passage.OnPassageUsed += TransitionBetweenScenes;
            if (passage.ID == idForEntrancePassageFromLastScene)
            {
                if(alreadyFoundEntrance)
                {
                    Debug.LogWarning("WARNING: Found multiple valid entrances with the same ID");
                }
                else
                {
                    entrancePassageFromLastScene = passage;
                    alreadyFoundEntrance = true;
                }
            }
        }

        if (entrancePassageFromLastScene == null && !isEmptyStartupScene)
        {
            Debug.Log("Warning: connecting passageway provided as the starting point an ID that does not exist in this scene.");
        }

    }

    private void SetUpPlayer()
    {
        if (!isEmptyStartupScene)
        {
            player.transform.position = entrancePassageFromLastScene.transform.position;
            player.Restart();
        }
        DontDestroyOnLoad(player.gameObject);
    }

    private void SetUpHUD()
    {
        DontDestroyOnLoad(HUDCanvas.gameObject);
    }

    private void CollectSceneInfo()
    {
        if(!isEmptyStartupScene)
        {

        }
    }

    private void SetUpStartupScene()
    {
        isEmptyStartupScene = false;
        gameStateInfo = new GameState();
        gameStateInfo.hasBubble = true;
    }

    private void OnOldSceneUnload()
    {
        foreach(ScenePassageway passage in scenePassageways)
        {
            passage.OnPassageUsed -= TransitionBetweenScenes;
        }


    }

[System.Serializable]
    public struct GameState
    {
        public bool hasBubble;
    }

}

