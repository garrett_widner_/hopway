﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseDinger : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip openDing;
    public AudioClip closeDing;
    public PauseManager pauseManager;

    private void OnEnable()
    {
        pauseManager.OnPaused += DingOpen;
        pauseManager.OnUnpause += DingClosed;
    }

    private void OnDisable()
    {
        pauseManager.OnPaused -= DingOpen;
        pauseManager.OnUnpause -= DingClosed;
    }

    private void DingOpen()
    {
        audioSource.Stop();
        audioSource.clip = openDing;
        audioSource.Play();
    }

    private void DingClosed()
    {
        audioSource.Stop();
        audioSource.clip = closeDing;
        audioSource.Play();
    }
}
