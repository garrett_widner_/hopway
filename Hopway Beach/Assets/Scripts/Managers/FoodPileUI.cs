﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodPileUI : MonoBehaviour
{
    public StatusBar foodBar;
    public PlayerPickupCollector pickupCollector;
    public Image img;
    public Vector3 shakeAmount;
    public float shakeDuration;
    public float timeBetweenChangeTriggerAndIconChange = 3.5f;
    public FoodPileIcon[] foodPileIcons;
    private List<FoodPileIcon> foodPileIconsList;
    public Sprite emptySprite;

    private int minIndexForGameEnd = 3;
    private int currentEnding = 1;

    private int currentFoodIconIndex = 0;

    private bool nextUpgradeCanHappen = true;

    private bool shouldDeleteFoodIcon = false;
    private FoodPileIcon nextIconToDelete;

    public delegate void FoodPileAction();
    public event FoodPileAction OnTimeForFoodPileUpgrade;

    private bool canShakeAgain = true;

    public int GetCurrentEnding
    {
        get
        {
            if(currentFoodIconIndex < minIndexForGameEnd)
            {
                return 0;
            }
            return currentEnding;
        }
    }

    private void OnEnable()
    {
        pickupCollector.OnPickedUp += CheckForFoodIconChange;
    }

    private void OnDisable()
    {
        pickupCollector.OnPickedUp -= CheckForFoodIconChange;
    }

    public Sprite GetCurrentFoodIcon()
    {
        return foodPileIcons[currentFoodIconIndex].foodSprite;
    }

    public void SetSpriteToEmpty()
    {
        img.sprite = foodPileIconsList[0].foodSprite;
    }

    private void Start()
    {
        foodPileIconsList = new List<FoodPileIcon>(foodPileIcons);
    }

    private void CheckForFoodIconChange()
    {
        //FoodPileIcon iconToDelete = new FoodPileIcon();
        //bool foundDeletableIcon = false;

        for (int i = currentFoodIconIndex; i < foodPileIconsList.Count - 1; i++)
        {
            if(nextUpgradeCanHappen)
            {
                if (foodBar.CurrentStatLevel >= foodPileIconsList[i].minToMovePast)
                {
                    //print("Current Stats: " + foodBar.CurrentStatLevel + "  Min food for display: " + foodPileIconsList[i].minToMovePast);
                    Invoke("ChangeFoodIcon", timeBetweenChangeTriggerAndIconChange);
                    nextUpgradeCanHappen = false;

                    if (OnTimeForFoodPileUpgrade != null)
                    {
                        OnTimeForFoodPileUpgrade();
                    }
                }
            }
        }

        if (canShakeAgain)
        {
            iTween.PunchPosition(gameObject, shakeAmount, shakeDuration);
            canShakeAgain = false;
            Invoke("AllowShaking", shakeDuration);
        }

        /*
        foreach (FoodPileIcon icon in foodPileIconsList)
        {
            if(foodBar.CurrentStatLevel >= icon.minFoodForDisplay && !shouldDeleteFoodIcon)
            {
                //img.sprite = icon.foodSprite;
                //iconToDelete = icon;
                //foundDeletableIcon = true;
                Invoke("ChangeFoodIcon", timeBetweenChangeTriggerAndIconChange);

                shouldDeleteFoodIcon = true;
                nextIconToDelete = icon;


                if(OnTimeForFoodPileUpgrade != null)
                {
                    OnTimeForFoodPileUpgrade();
                }
            }
        }
        */

        /*
        if (shouldDeleteFoodIcon)
        {
            //foodPileIconsList.Remove(iconToDelete);
            
            if(canShakeAgain)
            {
                iTween.PunchPosition(gameObject, shakeAmount, shakeDuration);
                canShakeAgain = false;
                Invoke("AllowShaking", shakeDuration);
            }
            
        }
        else
        {
            iTween.PunchPosition(gameObject, shakeAmount, shakeDuration);
            if(canShakeAgain)
            {
                canShakeAgain = false;
                Invoke("AllowShaking", shakeDuration);
            }
        }
        */
    }

    private void ChangeFoodIcon()
    {
        currentFoodIconIndex++;
        //print("Change icon index: " + currentFoodIconIndex);
        img.sprite = foodPileIconsList[currentFoodIconIndex].foodSprite;
        nextUpgradeCanHappen = true;
        //shouldDeleteFoodIcon = false;
        //foodPileIconsList.Remove(nextIconToDelete);

        UpdateCurrentEnding();
    }

    private void UpdateCurrentEnding()
    {
        if(currentFoodIconIndex > minIndexForGameEnd)
        {
            currentEnding++;
        }
    }

    private void AllowShaking()
    {
        canShakeAgain = true;
    }

    [System.Serializable]
    public struct FoodPileIcon
    {
        public Sprite foodSprite;
        public int minToMovePast;
    }
}
