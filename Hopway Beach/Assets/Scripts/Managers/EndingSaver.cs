﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;

public class EndingSaver : MonoBehaviour
{
    public static EndingSaver Instance;

    [HideInInspector]
    public SeenEndings seenEndings;

    public delegate void EndingAction();
    public EndingAction OnEndingsEdited;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += SetUpScene;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SetUpScene;
    }

    public bool AreAnyEndingsUnlocked
    {
        get
        {
            return (seenEndings.ending1 || seenEndings.ending2 || seenEndings.ending3 || seenEndings.ending4);
        }
    }

    private void Awake()
    {
        if(Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if(Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void SetUpScene(Scene scene, LoadSceneMode lsm)
    {
        //print("Ending Saver SetUpScene called");
        Awake();
        seenEndings = new SeenEndings(false);
        LoadData();
    }

    public void UnlockEnding(int ending)
    {
        switch(ending)
        {
            case 1:
                seenEndings.ending1 = true;
                break;
            case 2:
                seenEndings.ending2 = true;
                break;
            case 3:
                seenEndings.ending3 = true;
                break;
            case 4:
                seenEndings.ending4 = true;
                break;
            default:
                Debug.LogWarning("Warning: ending numbering is wrong");
                break;
        }

        SaveData();
    }

    public void SaveData()
    {
        if(!Directory.Exists("Stonebridge Sea_Data"))
        {
            Directory.CreateDirectory("Stonebridge Sea_Data");
        }

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = File.Create("Stonebridge Sea_Data/save_data.binary");

        formatter.Serialize(saveFile, seenEndings);

        saveFile.Close();

        if(OnEndingsEdited!= null)
        {
            OnEndingsEdited();
        }
    }

    public void LoadData()
    {
        /*
        if(Directory.Exists("SaveData") && File.Exists("SaveData/save.binary"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream saveFile = File.Open("SaveData/save.binary", FileMode.Open);

            seenEndings = (SeenEndings)formatter.Deserialize(saveFile);

            saveFile.Close();
        }
        */

        if (Directory.Exists("Stonebridge Sea_Data") && File.Exists("Stonebridge Sea_Data/save_data.binary"))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream saveFile = File.Open("Stonebridge Sea_Data/save_data.binary", FileMode.Open);

            seenEndings = (SeenEndings)formatter.Deserialize(saveFile);

            saveFile.Close();
        }

        //FindObjectOfType<EndingsUIDisplayer>().RunSetup();

        //print("Load Data called from EndingSaver");
        if (OnEndingsEdited != null)
        {
            OnEndingsEdited();
        }
    }

    [System.Serializable]
    public struct SeenEndings
    {
        public bool ending1, ending2, ending3, ending4;

        public bool[] GetEndingsArray()
        {
            return new bool[] { ending1, ending2, ending3, ending4 };
        }

        public SeenEndings(bool seen1, bool seen2, bool seen3, bool seen4)
        {
            ending1 = seen1;
            ending2 = seen2;
            ending3 = seen3;
            ending4 = seen4;
        }

        public SeenEndings(bool seenAll)
        {
            ending1 = ending2 = ending3 = ending4 = seenAll;
        }
    }

}
