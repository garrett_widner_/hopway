﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VignetteSetter : MonoBehaviour
{
    public GameObject[] vignettePrefabs;
    public Camera mainCamera;

    [HideInInspector]
    public Transform vignetteSpawnTransform;
    public Transform vignetteUIFinalTransform;

    private GameObject vignette;

    public void SpawnCorrectVignette(int ending, Camera main)
    {
        if(ending <= vignettePrefabs.Length)
        {
            vignette = Instantiate(vignettePrefabs[ending - 1], vignetteSpawnTransform.position, Quaternion.identity);
        }
        else
        {
            Debug.LogWarning("Warning: Ending number greater than number of vignette Prefabs");
        }

        mainCamera = main;
    }

    public void PlaceVignette()
    {
        Vector3 point = mainCamera.ScreenToWorldPoint(vignetteUIFinalTransform.position);
        vignette.transform.position = new Vector3(point.x, point.y, vignette.transform.position.z);
        vignette.transform.SetParent(mainCamera.transform);
            //vignetteUIFinalTransform.position.x, vignetteUIFinalTransform.position.y, vignette.transform.position.z);
    }

    public void DeleteVignette()
    {
        Destroy(vignette);
    }
}
