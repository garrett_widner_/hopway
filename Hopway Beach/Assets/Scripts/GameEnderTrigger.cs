﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnderTrigger : MonoBehaviour
{
    public PilePlacer pile;
    public CutsceneSequencer endCutsceneSequencer;

    private void OnEnable()
    {
        pile.OnPilePlaced += StartFinalCutscene;
    }

    private void OnDisable()
    {
        pile.OnPilePlaced -= StartFinalCutscene;
    }

    private void StartFinalCutscene()
    {
        endCutsceneSequencer.BeginCutscene();
    }

}
