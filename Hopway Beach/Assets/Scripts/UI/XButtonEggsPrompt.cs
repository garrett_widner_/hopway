﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class XButtonEggsPrompt : ButtonPressPrompt
{
    public BoxCollider2D characterTriggerArea;
    public LayerMask characterLayer;
    public GameObject egg1;
    public GameObject egg2;
    public GameObject egg3;
    public PromptImageChanger promptImageChanger;
    public StartButtonPrompt startButtonPrompt;

    protected override void CheckForPromptStart()
    {
        Collider2D foundCharacter = Physics2D.OverlapArea(characterTriggerArea.bounds.max, characterTriggerArea.bounds.min, characterLayer);
        if (foundCharacter != null)
        {
            if (egg1 != null || egg2 != null || egg3 != null)
            {
                StartPrompt();
                promptImageChanger.UsePickupPrompt();
            }
        }
    }

    protected override void CheckForPromptEnd()
    {
        if (egg1 == null && egg2 == null && egg3 == null)
        {
            EndPrompt();
            startButtonPrompt.SchedulePromptStart();
        }
    }

    protected override void CheckForAnimationPause()
    {
        Collider2D foundCharacter = Physics2D.OverlapArea(characterTriggerArea.bounds.max, characterTriggerArea.bounds.min, characterLayer);
        if (foundCharacter == null)
        {
            PauseAnimation();
        }
    }

    protected override void CheckForAnimationResume()
    {
        Collider2D foundCharacter = Physics2D.OverlapArea(characterTriggerArea.bounds.max, characterTriggerArea.bounds.min, characterLayer);
        if (foundCharacter != null)
        {
            ResumeAnimation();
            promptImageChanger.UsePickupPrompt();
        }
    }
}
