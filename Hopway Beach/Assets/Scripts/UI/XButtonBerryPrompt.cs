﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XButtonBerryPrompt : ButtonPressPrompt
{
    public StartButtonPrompt startButtonPrompt;
    public BoxCollider2D characterTriggerArea;
    public LayerMask characterLayer;
    public GameObject berry;
    public PromptImageChanger promptImageChanger;

    protected override void CheckForPromptStart()
    {
        if(!promptIsPlaying && !promptHasPlayed)
        {
            Collider2D foundCharacter = Physics2D.OverlapArea(characterTriggerArea.bounds.max, characterTriggerArea.bounds.min, characterLayer);
            if (foundCharacter != null)
            {
                if (berry != null)
                {
                    //startButtonPrompt.OtherPromptWasStarted();
                    //Invoke("StartSelf", 2f);
                    StartPrompt();
                    promptImageChanger.UsePickupPrompt();
                }
            }
        }
    }

    private void StartSelf()
    {
        StartPrompt();
        promptImageChanger.UsePickupPrompt();
    }

    protected override void CheckForPromptEnd()
    {
        if(berry == null)
        {
            EndPrompt();
        }
    }

    protected override void CheckForAnimationPause()
    {
        Collider2D foundCharacter = Physics2D.OverlapArea(characterTriggerArea.bounds.max, characterTriggerArea.bounds.min, characterLayer);
        if (foundCharacter == null)
        {
            PauseAnimation();
        }
    }

    protected override void CheckForAnimationResume()
    {
        Collider2D foundCharacter = Physics2D.OverlapArea(characterTriggerArea.bounds.max, characterTriggerArea.bounds.min, characterLayer);
        if (foundCharacter != null)
        {
            ResumeAnimation();
            promptImageChanger.UsePickupPrompt();
        }
    }
}
