﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndingsUIDisplayer : MonoBehaviour
{
    public EndingDisplay[] endings;

    //private bool isSetup = false;

    /*
    public void RunSetup()
    {
        //print("Setup run on EndingsUIDisplayer");
        if(!isSetup)
        {
            //print("EndingUIDisplayer is now set up- should update images");
            isSetup = true;
            
        }
    }
    */

    private void OnEnable()
    {
        if (EndingSaver.Instance != null)
        {
            EndingSaver.Instance.OnEndingsEdited += UpdateEndingUIImages;
        }
    }

    private void OnDisable()
    {
        if(EndingSaver.Instance != null)
        {
            EndingSaver.Instance.OnEndingsEdited -= UpdateEndingUIImages;
        }
    }

    public void UpdateEndingUIImages()
    {
        //print("Endings data pulled into UI Displayer and images updated");
        bool[] seenList = EndingSaver.Instance.seenEndings.GetEndingsArray();

        for(int i = 0; i < endings.Length; i++)
        {
            if(seenList[i])
            {
                //print("Ending number " + (i + 1) + " unlocked");
                endings[i].Unlock();
            }
            else
            {
                endings[i].Lock();
            }
        }
    }

    public void ShowAll()
    {
        foreach(EndingDisplay ending in endings)
        {
            ending.MakeVisible();
        }
    }

    public void HideAll()
    {
        foreach (EndingDisplay ending in endings)
        {
            ending.MakeInvisible();
        }
    }

    [System.Serializable]
    public class EndingDisplay
    {
        [SerializeField]
        private int number;
        [SerializeField]
        private Image img;
        [SerializeField]
        private Sprite locked;
        [SerializeField]
        private Sprite unlocked;

        public int Number
        {
            get
            {
                return number;
            }
        }
        
        public void MakeInvisible()
        {
            img.enabled = false;
        }

        public void MakeVisible()
        {
            img.enabled = true;
        }

        public void Unlock()
        {
            img.sprite = locked;
        }

        public void Lock()
        {
            img.sprite = unlocked;
        }
    }
}
