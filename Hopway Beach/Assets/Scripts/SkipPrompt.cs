﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipPrompt : ButtonPressPrompt
{
    public CutsceneSequencer startCutscene;
    public CutsceneSequencer endCutscene;

    public PromptImageChanger promptImageChanger;

    private PlayerActions playerActions;

    private bool startCutsceneIsPlaying = false;
    private bool endCutsceneIsPlaying = false;

    private bool promptCanBeShown = false;

    private int pressCounter = 0;
    private int minPressesForPromptDisplay = 6;
    private float pressRemovalTime = 1.5f;

    public float timeBeforeCutsceneStartAndPromptDisplay = 4f;

    private void OnEnable()
    {
        startCutscene.OnCutsceneStart += AllowPrompt;
        startCutscene.OnCutsceneEnd += DisallowPrompt;
        endCutscene.OnCutsceneStart += AllowPrompt;
        endCutscene.OnCutsceneEnd += DisallowPrompt;
    }

    private void OnDisable()
    {
        startCutscene.OnCutsceneStart -= AllowPrompt;
        startCutscene.OnCutsceneEnd -= DisallowPrompt;
        endCutscene.OnCutsceneStart -= AllowPrompt;
        endCutscene.OnCutsceneEnd -= DisallowPrompt;
    }

    protected override void Start()
    {
        base.Start();
        playerActions = PlayerActions.CreateWithDefaultBindings();
    }

    protected override void CheckForAnimationPause()
    {
    }

    protected override void CheckForAnimationResume()
    {
    }

    protected override void CheckForPromptStart()
    {
        if(promptCanBeShown && !promptIsPlaying)
        {
            //print("Checking whether prompt can be shown");
            if(playerActions.Pickup.WasPressed)
            {
                ButtonPressed();
            }

            if(pressCounter >= minPressesForPromptDisplay)
            {
                promptImageChanger.UseSkipPrompt();
                //print("Skip Prompt Started ---------------");
                StartPrompt();
            }
        }
    }

    protected override void StartPrompt()
    {
        base.StartPrompt();
        animationIsPlaying = false;
        shouldStopAnimation = false;
    }

    protected override void EndPrompt()
    {
        //animationIsPlaying = true;
        base.EndPrompt();
        promptIsPlaying = true;

    }

    protected override void CheckForPromptEnd()
    {

    }

    private void AllowPrompt()
    {
        //print("The skip prompt can be shown");
        promptCanBeShown = true;
        promptIsPlaying = false;
    }

    private void DisallowPrompt()
    {
        //print("The skip prompt canNOT be shown");
        EndPrompt();
        promptCanBeShown = false;
        promptHasPlayed = false;
    }

    public void ButtonPressed()
    {
        pressCounter++;
        Invoke("RemovePress", pressRemovalTime);
    }

    public void RemovePress()
    {
        pressCounter--;
        if(pressCounter < 0)
        {
            pressCounter = 0;
        }
    }


}
