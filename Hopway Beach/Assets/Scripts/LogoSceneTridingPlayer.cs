﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoSceneTridingPlayer : MonoBehaviour
{
    public AudioClip triDing;
    public float playDingAfterStart;
    public AudioSource audioSource;

    private void Start()
    {
        Invoke("PlayDing", playDingAfterStart);
    }

    private void PlayDing()
    {
        audioSource.PlayOneShot(triDing);
    }

}
