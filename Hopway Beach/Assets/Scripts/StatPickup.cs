﻿using UnityEngine;
using System.Collections;

public class StatPickup : MonoBehaviour 
{
    public StatusTracker.Type pickupType;
    public float statIncrease;
    public bool isElevated = false;

    public void DestroySelf()
    {
        Destroy(this.gameObject);
    }
}
