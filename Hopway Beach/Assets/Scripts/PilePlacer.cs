﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilePlacer : MonoBehaviour
{
    public FoodPileUI foodPileUI;
    public SpriteRenderer spriteRenderer;

    public delegate void PilePlaceAction();
    public event PilePlaceAction OnPilePlaced;

    private bool hasBeenPlaced = false;

    public void PlacePile()
    {
        if(!hasBeenPlaced)
        {
            hasBeenPlaced = true;
            spriteRenderer.sprite = foodPileUI.GetCurrentFoodIcon();
            foodPileUI.SetSpriteToEmpty();
            if (OnPilePlaced != null)
            {
                OnPilePlaced();
            }
        }
        
    }

}
