﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBurst : MonoBehaviour
{
    public GameObject burst;

    public float burstScale;
    public float scaleDuration;
    public float scaleHoldDuration;
    public FoodPileUI foodPile;

    private Hashtable tweenHash = new Hashtable();
    private Vector3 originalScale;

    public string easeInType = "easeOutQuad";
    public string easeOutType = "easeInQuad";

    private void OnEnable()
    {
        foodPile.OnTimeForFoodPileUpgrade += Burst;
    }

    private void OnDisable()
    {
        foodPile.OnTimeForFoodPileUpgrade -= Burst;
    }

    private void Start()
    {
        originalScale = transform.localScale;
    }

    private void Burst()
    {
        tweenHash.Clear();
        tweenHash.Add("easetype", easeInType);
        tweenHash.Add("time", scaleDuration);

        tweenHash.Add("scale", new Vector3(burstScale, burstScale, transform.localScale.z));

        iTween.ScaleTo(burst, tweenHash);

        CancelInvoke("UnBurst");
        Invoke("UnBurst", scaleDuration + scaleHoldDuration);
    }

    private void UnBurst()
    {
        tweenHash.Clear();
        tweenHash.Add("easetype", easeOutType);
        tweenHash.Add("time", scaleDuration);
        tweenHash.Add("scale", new Vector3(originalScale.x, originalScale.y, originalScale.z));

        iTween.ScaleTo(burst, tweenHash);
    }

}
