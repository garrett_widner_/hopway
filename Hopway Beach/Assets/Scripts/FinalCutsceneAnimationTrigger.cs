﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalCutsceneAnimationTrigger : MonoBehaviour
{
    public Animator ilia;
    public Animator nat;
    public PostCutsceneMovementStarter characterMovementHandler;

    public string triggerName;
    public CutsceneSequencer endCutscene;

    private void OnEnable()
    {
        endCutscene.OnCutsceneStart += TriggerCutsceneStart;
    }

    private void OnDisable()
    {
        endCutscene.OnCutsceneStart -= TriggerCutsceneStart;
    }

    private void TriggerCutsceneStart()
    {
        characterMovementHandler.DisableCharacterControllerAndMovement();
        ilia.SetTrigger(triggerName);
        nat.SetTrigger(triggerName);
    }

}
