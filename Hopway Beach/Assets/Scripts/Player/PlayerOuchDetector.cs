﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOuchDetector : MonoBehaviour
{
    public BoxCollider2D ouchCollider;
    public LayerMask whatCausesOuch;
    public PlayerHopHandler playerHopHandler;

    public delegate void OuchAction();
    public OuchAction OnOuch;

    private void Update()
    {
        if(playerHopHandler.IsInHopSequence && !playerHopHandler.playerHopMover.IsHopping)
        {
            Collider2D foundCollider = Physics2D.OverlapArea(ouchCollider.bounds.min, ouchCollider.bounds.max, whatCausesOuch);
            if(foundCollider != null)
            {
                playerHopHandler.HopBackToStart();
                if(OnOuch != null)
                {
                    OnOuch();
                }
            }
        }
    }
}
