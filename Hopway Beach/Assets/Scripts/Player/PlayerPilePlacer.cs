﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPilePlacer : MonoBehaviour
{
    public LayerMask whatIsPileLocation;
    public CircleCollider2D pileCheckArea;
    public StatusBar foodBar;
    public int minimumFoodForPlacement;

    [Header("Dialogue")]
    public Dialogue findMoreFoodDialogue;
    public DialogueManager dialogueManager;
    public PostCutsceneMovementStarter postCutsceneMovementStarter;
    public PauseManager pauseManager;

    private bool dialogueIsPlaying = false;

    private void OnEnable()
    {
        dialogueManager.OnDialogueEnd += EndDialogue;
    }

    private void OnDisable()
    {
        dialogueManager.OnDialogueEnd -= EndDialogue;
    }

    public void CheckForPilePlacement()
    {
        if(!dialogueIsPlaying)
        {
            Collider2D foundCollider = Physics2D.OverlapCircle(transform.position, pileCheckArea.radius, whatIsPileLocation);
            if (foundCollider != null)
            {
                PilePlacer pilePlacer = foundCollider.GetComponent<PilePlacer>();
                if (pilePlacer != null)
                {
                    if (foodBar.CurrentStatLevel >= minimumFoodForPlacement)
                    {
                        pilePlacer.PlacePile();
                    }
                    else
                    {
                        StartDialogue();
                    }
                }
            }
        }
    }

    private void StartDialogue()
    {
        dialogueIsPlaying = true;
        postCutsceneMovementStarter.DisableMovement();
        pauseManager.DisablePausing();
        dialogueManager.StartDialogue(findMoreFoodDialogue);
    }

    private void EndDialogue()
    {
        if(dialogueIsPlaying)
        {
            postCutsceneMovementStarter.EnableMovement();
            pauseManager.EnablePausing();
            dialogueIsPlaying = false;
        }
    }

}
