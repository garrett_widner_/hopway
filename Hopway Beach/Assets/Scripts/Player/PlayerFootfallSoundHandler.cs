﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFootfallSoundHandler : SingleSoundPlayer
{
    public AudioClip beachRunClip;
    public AudioClip hopFootfallClip;

    public GroundMover playerGroundMover;
    public HopMover playerHopMover;

    private void OnEnable()
    {
        playerHopMover.OnHopEnded += PlayHopFootfall;
        playerGroundMover.OnMovementStarted += StartBeachRun;
        playerGroundMover.OnMovementStopped += StopBeachRun;
    }

    private void OnDisable()
    {
        playerHopMover.OnHopEnded -= PlayHopFootfall;
        playerGroundMover.OnMovementStarted -= StartBeachRun;
        playerGroundMover.OnMovementStopped -= StopBeachRun;
    }

    private void PlayHopFootfall()
    {
        soundSource.Stop();
        soundSource.clip = hopFootfallClip;
        PlaySoundWithRandomPitch();
    }

    private void StartBeachRun()
    {
        soundSource.Stop();
        soundSource.clip = beachRunClip;
        soundSource.loop = true;
        PlaySound();
    }

    private void StopBeachRun()
    {
        soundSource.Stop();
        soundSource.loop = false;
    }

}
