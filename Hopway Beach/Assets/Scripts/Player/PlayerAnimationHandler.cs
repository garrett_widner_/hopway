﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationHandler : MonoBehaviour
{
    public GroundMover groundMover;
    public Animator animator;
    public HopMover hopMover;

    private int isMovingHash = Animator.StringToHash("IsMoving");
    private int isHoppingHash = Animator.StringToHash("IsHopping");
    private int isOnLeftLegHash = Animator.StringToHash("IsLeftHop");
    private int xDirectionHash = Animator.StringToHash("XDirection");
    private int yDirectionHash = Animator.StringToHash("YDirection");
    private int isHopHalfOverHash = Animator.StringToHash("IsHopHalfOver");

    private bool isOnLeftLeg;

    private void OnEnable()
    {
        hopMover.OnHopEnded += SwitchLeg;
        hopMover.OnHopStarted += HopStartAnimation;
        hopMover.OnHopHalfOver += HopMidAnimation;
    }

    private void OnDisable()
    {
        hopMover.OnHopEnded -= SwitchLeg;
        hopMover.OnHopStarted -= HopStartAnimation;
        hopMover.OnHopHalfOver -= HopMidAnimation;
    }

    public void Update()
    {
        if(groundMover.IsMoving != animator.GetBool(isMovingHash))
        {
            animator.SetBool(isMovingHash, groundMover.IsMoving);
        }

        if (animator.GetInteger(xDirectionHash) != (int)groundMover.FaceDirection.x)
        {
            animator.SetInteger(xDirectionHash, (int)groundMover.FaceDirection.x);
        }
        if (animator.GetInteger(yDirectionHash) != (int)groundMover.FaceDirection.y)
        {
            animator.SetInteger(yDirectionHash, (int)groundMover.FaceDirection.y);
        }

        if(hopMover.IsHopping != animator.GetBool(isHoppingHash))
        {
            animator.SetBool(isHoppingHash, hopMover.IsHopping);
        }
    }

    private void SwitchLeg()
    {
        isOnLeftLeg = !isOnLeftLeg;
        animator.SetBool(isOnLeftLegHash, isOnLeftLeg);
    }

    private void HopStartAnimation()
    {
        animator.SetBool(isHopHalfOverHash, false);
    }

    private void HopMidAnimation()
    {
        animator.SetBool(isHopHalfOverHash, true);
    }


}
