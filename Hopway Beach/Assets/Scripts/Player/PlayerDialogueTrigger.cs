﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDialogueTrigger : MonoBehaviour
{
    //Checks for player entering into dialogues and triggers those dialogues
    public LayerMask whatIsDialogueTrigger;
    public CircleCollider2D dialogueCheckArea;

    public DialogueManager dialogueManager;

    private bool canCheck = true;

    private void OnEnable()
    {
        dialogueManager.OnDialogueEnd += EnableDialogueChecking;
    }

    private void OnDisable()
    {
        dialogueManager.OnDialogueEnd -= EnableDialogueChecking;
    }

    public void CheckForDialogue()
    {
        if(canCheck)
        {
            Collider2D foundCollider = Physics2D.OverlapCircle(transform.position, dialogueCheckArea.radius);
            if (foundCollider != null)
            {
                MidgameDialogueManager natalieDialogue = foundCollider.GetComponent<MidgameDialogueManager>();
                if (natalieDialogue != null)
                {
                    canCheck = false;
                    natalieDialogue.PlayDialogue();
                }
            }
        }
        
    }

    private void EnableDialogueChecking()
    {
        canCheck = true;
    }

}
