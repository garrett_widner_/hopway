﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHopHandler : MonoBehaviour
{
    //If running and facing in correct direction, jump.

    public GroundMover groundMover;
    public BoxCollider2D playerCollider;
    public LayerMask hoppableLayer;
    public HopMover playerHopMover;

    public PlayerActions playerActions;

    private Hoppable currentHoppable;
    private Hoppable nextHoppable;
    private Hoppable startingHoppable;

    private bool canHop = true;

    private bool isInHopSequence = false;
    public bool IsInHopSequence
    {
        get
        {
            return isInHopSequence;
        }
    }

    public void DisallowHopping()
    {
        canHop = false;
    }

    public void AllowHopping()
    {
        canHop = true;
    }

    //private Collider2D previousFoundCollider;

    public delegate void HopHandlerAction();
    public event HopHandlerAction OnHopChainStarted;
    public event HopHandlerAction OnHopChainEnded;

    private bool isSetup = false;

    

    private void OnEnable()
    {
        playerHopMover.OnHopEnded += HopEnded;
    }

    private void OnDisable()
    {
        playerHopMover.OnHopEnded -= HopEnded;
    }

    public void RunSetup(PlayerActions actions)
    {
        playerActions = actions;
        isSetup = true;
    }

    private void Update()
    {
        //Make sure to put player above enemies draw-wise and turn off its collider.
        if (isSetup && canHop)
        {
            if (!isInHopSequence)
            {
                CheckForHopChainStart();
            }
            else if (isInHopSequence && !playerHopMover.IsHopping)
            {
                CheckForNextHopInChain();
            }
        }
    }

    public void HopBackToStart()
    {
        if(!playerHopMover.IsHopping && isInHopSequence)
        {
            HopStarted(startingHoppable, true);
        }
    }

    private void CheckForHopChainStart()
    {
        Collider2D foundCollider = Physics2D.OverlapArea(playerCollider.bounds.max, playerCollider.bounds.min, hoppableLayer);
        if (foundCollider != null)
        {
            Hoppable foundHoppable = foundCollider.GetComponent<Hoppable>();
            if (foundHoppable != null)
            {
                if (foundHoppable.isStartingPad)
                {
                    if (foundHoppable.IsThereAHoppableInDirection(groundMover.FaceDirection))
                    {
                        Hoppable nextHoppable = foundHoppable.GetHoppableInDirection(groundMover.FaceDirection);
                        startingHoppable = nextHoppable;
                        HopStarted(nextHoppable);
                    }
                }
            }
        }
    }

    private void CheckForNextHopInChain()
    {
        Vector2 desiredMoveDirection = playerActions.Move.Value.ClosestCardinalDirectionNormalized();
        if (playerActions.Move.IsPressed && currentHoppable.IsThereAHoppableInDirection(desiredMoveDirection))
        {
            HopStarted(currentHoppable.GetHoppableInDirection(desiredMoveDirection));
        }
    }

    private void HopStarted(Hoppable destinationHoppable, bool isOuchHop = false)
    {
        if(!isInHopSequence)
        {
            StartHopChain();
        }
        
        currentHoppable = null;
        nextHoppable = destinationHoppable;

        if(!isOuchHop)
        {
            playerHopMover.StartDurationBasedHop(destinationHoppable.HopPoint);
        }
        else
        {
            playerHopMover.StartDurationBasedHop(destinationHoppable.HopPoint, 3f, .8f);
        }
    }

    private void StartHopChain()
    {
        isInHopSequence = true;
        if (OnHopChainStarted != null)
        {
            OnHopChainStarted();
        }
    }

    private void HopEnded()
    {
        if (nextHoppable.isStartingPad)
        {
            EndHopChain();
        }

        if (nextHoppable.isStartingPointForKnockback)
        {
            startingHoppable = nextHoppable;
        }


        currentHoppable = nextHoppable;
        nextHoppable = null;
    }

    private void EndHopChain()
    {
        isInHopSequence = false;
        if (OnHopChainEnded != null)
        {
            OnHopChainEnded();
        }
        startingHoppable = null;
    }



}
