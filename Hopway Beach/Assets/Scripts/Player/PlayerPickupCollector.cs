﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerPickupCollector : MonoBehaviour 
{
    public LayerMask pickupLayerMask;
    public CircleCollider2D pickupArea;
    public StatusBar affectedBar;
    public StatusTracker.Type pickupType;
    public PlayerHopHandler playerHopHandler;

    bool pickupWasFound = false;

    public delegate void PickupAction();
    public PickupAction OnPickedUp;

    private bool pickingUpIsLocked = false;

    public void LockPickup()
    {
        pickingUpIsLocked = true;
    }

    public void UnlockPickup()
    {
        pickingUpIsLocked = false;
    }

    public void CheckForPickup()
    {
        if(!pickingUpIsLocked)
        {
            //print("Told to check for pickup");
            Collider2D[] pickupColliders = Physics2D.OverlapCircleAll(transform.position, pickupArea.radius, pickupLayerMask);
            pickupWasFound = false;
            foreach (Collider2D collider in pickupColliders)
            {
                StatPickup pickup = collider.GetComponent<StatPickup>();
                if (pickup != null && !pickupWasFound)
                {
                    if (pickup.pickupType == pickupType)
                    {
                        if (!affectedBar.IsFull || true)
                        {
                            if ((pickup.isElevated && playerHopHandler.IsInHopSequence) || (!pickup.isElevated && !playerHopHandler.IsInHopSequence))
                            {
                                //print("Picked up");
                                if (OnPickedUp != null)
                                {
                                    OnPickedUp();
                                }
                                affectedBar.HealImmediate((int)pickup.statIncrease);
                                pickup.DestroySelf();
                                pickupWasFound = true;
                            }
                        }
                    }
                }
            }
        }
        
    }
}
