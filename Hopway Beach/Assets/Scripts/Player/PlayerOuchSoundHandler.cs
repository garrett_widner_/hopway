﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOuchSoundHandler : SingleSoundPlayer
{
    public List<AudioClip> ouches = new List<AudioClip>();

    public PlayerOuchDetector playerOuch;

    public void OnEnable()
    {
        playerOuch.OnOuch += PlayRandomOuch;
    }

    public void OnDisable()
    {
        playerOuch.OnOuch -= PlayRandomOuch;
    }

    public void PlayRandomOuch()
    {
        int randomInt = Random.Range(0, ouches.Count);

        soundSource.clip = ouches[randomInt];
        PlaySound();
    }

}
