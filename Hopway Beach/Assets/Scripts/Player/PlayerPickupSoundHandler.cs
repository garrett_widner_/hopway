﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickupSoundHandler : SingleSoundPlayer
{
    public PlayerPickupCollector pickupCollector;

    private void OnEnable()
    {
        pickupCollector.OnPickedUp += PlaySoundWithRandomPitch;
    }

    private void OnDisable()
    {
        pickupCollector.OnPickedUp -= PlaySoundWithRandomPitch;
    }

}
