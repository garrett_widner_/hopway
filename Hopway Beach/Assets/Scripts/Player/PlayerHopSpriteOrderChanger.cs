﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHopSpriteOrderChanger : MonoBehaviour
{
    public PlayerHopHandler playerHopHandler;
    public string underLayer;
    public string overLayer;
    public SpriteRenderer spriteRenderer;

    public void OnEnable()
    {
        playerHopHandler.OnHopChainStarted += ChangeSpriteLayerToOver;
        playerHopHandler.OnHopChainEnded += ChangeSpriteLayerToUnder;
    }

    public void OnDisable()
    {
        playerHopHandler.OnHopChainStarted -= ChangeSpriteLayerToOver;
        playerHopHandler.OnHopChainEnded -= ChangeSpriteLayerToUnder;
    }

    private void ChangeSpriteLayerToOver()
    {
        spriteRenderer.sortingLayerName = overLayer;
    }

    private void ChangeSpriteLayerToUnder()
    {
        spriteRenderer.sortingLayerName = underLayer;
    }

}
