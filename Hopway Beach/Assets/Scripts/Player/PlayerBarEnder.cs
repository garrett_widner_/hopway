﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBarEnder : MonoBehaviour
{
    public StatusBar statBar;

    private void OnEnable()
    {
        statBar.OnStatLevelHitMax += CleanUpYourSins;
    }

    private void OnDisable()
    {
        statBar.OnStatLevelHitMax -= CleanUpYourSins;
    }

    private void CleanUpYourSins()
    {
        /*
        List<StatPickup> allPickups = new List<StatPickup>();
        allPickups = new List<StatPickup>(FindObjectsOfType<StatPickup>());
        foreach(StatPickup pickup in allPickups)
        {
            Destroy(pickup.gameObject);
        }
        */
        print("You collected the max!");
    }

    private void Update()
    {
        
    }

}
