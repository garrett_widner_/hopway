﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogoScenePresentsDisplayer : MonoBehaviour
{
    public Text text;
    public ScreenBlackout screenBlackout;
    public AudioSource audioSource;
    public AudioClip pop;
    public float showTextAfterSeconds = 1f;

    private void OnEnable()
    {
        screenBlackout.OnScreenCompletelyVisible += ShowTextInTime;
    }

    private void OnDisable()
    {
        screenBlackout.OnScreenCompletelyVisible -= ShowTextInTime;
    }

    private void ShowTextInTime()
    {
        Invoke("ShowText", showTextAfterSeconds);
    }

    private void ShowText()
    {
        audioSource.PlayOneShot(pop);
        text.enabled = true;
    }

}
