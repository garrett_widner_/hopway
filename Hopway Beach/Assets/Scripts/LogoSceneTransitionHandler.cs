﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoSceneTransitionHandler : MonoBehaviour
{
    public LogoSceneBlackoutHandler logoSceneBlackoutHandler;
    public string transitionSceneName;

    private void OnEnable()
    {
        logoSceneBlackoutHandler.OnLogoSequenceFinished += TriggerSceneChange;
    }

    private void OnDisable()
    {
        logoSceneBlackoutHandler.OnLogoSequenceFinished -= TriggerSceneChange;
    }

    private void TriggerSceneChange()
    {
        SceneManager.LoadSceneAsync(transitionSceneName);
    }
}
