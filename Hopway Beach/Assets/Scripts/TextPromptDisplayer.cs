﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextPromptDisplayer : MonoBehaviour
{
    public DialogueManager dialogueManager;
    public Image promptImage;
    public float fadeInTime = 0.6f;
    public float FadeOutTime = 0.1f;
    public float timeUntilPromptShow = 1.5f;

    private float timeUntilCanDisplayAfterStoppage = 10f;
    private bool canDisplay = true;

    private void OnEnable()
    {
        dialogueManager.OnBoxFullyVisible += PromptStartTrigger;
        dialogueManager.OnBoxInvisible += PromptEndTrigger;
    }

    private void OnDisable()
    {
        dialogueManager.OnBoxFullyVisible -= PromptStartTrigger;
        dialogueManager.OnBoxInvisible -= PromptEndTrigger;
    }

    private void Start()
    {
        promptImage.CrossFadeAlpha(0, 0.01f, true);
    }

    private void PromptStartTrigger()
    {
        if(canDisplay)
        {
            Invoke("FadeIn", timeUntilPromptShow);
        }
    }

    private void FadeIn()
    {
        promptImage.CrossFadeAlpha(.9f, fadeInTime, true);
    }

    private void PromptEndTrigger()
    {
        CancelInvoke("FadeIn");
        promptImage.CrossFadeAlpha(0, FadeOutTime, true);
    }

    public void StopDisplay()
    {
        canDisplay = false;
        Invoke("AllowDisplayAgain", timeUntilCanDisplayAfterStoppage);
        CancelInvoke("FadeIn");
        promptImage.CrossFadeAlpha(0, 0.001f, true);
    }

    public void AllowDisplayAgain()
    {
        canDisplay = true;
    }
}
