﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidgameDialogueManager : MonoBehaviour
{
    public PostCutsceneMovementStarter postCutsceneMovementStarter;
    public PauseManager pauseManager;
    public DialogueManager dialogueManager;
    public StartIslandSequencer startIslandSequencer;
    public CutsceneSequencer startCutsceneSequencer;
    public CutsceneSequencer endCutsceneSequencer;

    public Dialogue postOpeningDialogue;
    public Dialogue collectingDialogue;
    public Dialogue postTreeCutDialogue;
    public Dialogue raftBuiltDialogue;

    private bool isEnabled = false;

    private void OnEnable()
    {
        dialogueManager.OnDialogueEnd += EndDialogue;
        startCutsceneSequencer.OnCutsceneEnd += StartEnable;
        endCutsceneSequencer.OnCutsceneStart += EndDisable;
    }

    private void OnDisable()
    {
        dialogueManager.OnDialogueEnd -= EndDialogue;
        startCutsceneSequencer.OnCutsceneEnd -= StartEnable;
        endCutsceneSequencer.OnCutsceneStart -= EndDisable;


    }

    private void StartEnable()
    {
        isEnabled = true;
    }

    private void EndDisable()
    {
        isEnabled = false;
    }

    public void PlayDialogue()
    {
        postCutsceneMovementStarter.DisableMovement();
        pauseManager.DisablePausing();
        Dialogue currentDialogue = GetCurrentDialogue(startIslandSequencer.CurrentBuildingStage);
        dialogueManager.StartDialogue(currentDialogue);
    }

    public Dialogue GetCurrentDialogue(int currentBuildStage)
    {
        switch (currentBuildStage)
        {
            case 1:
                return postOpeningDialogue;
            case 2:
                return collectingDialogue;
            case 3:
                return postTreeCutDialogue;
            default:
                return raftBuiltDialogue;
        }
    }

    public void EndDialogue()
    {
        if(isEnabled)
        {
            postCutsceneMovementStarter.EnableMovement();
            pauseManager.EnablePausing();
        }
    }

}
