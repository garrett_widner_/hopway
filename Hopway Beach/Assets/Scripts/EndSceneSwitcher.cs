﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndSceneSwitcher : MonoBehaviour
{
    public int startSwitchAtSequenceOrder;
    public CutsceneSequencer endCutscene;

    private FinalSceneSetup setupper;

    public Image blackoutImage;
    public float blackoutSpeed;
    public Color colorFinal;
    public Color colorBegin;
    public Color currentColor;
    public string sceneToSwitchTo;
    public Image promptImage;
    public PromptImageChanger promptImageChanger;
    public ScreenBlackout blackOuter;

    [Header("Don't Destroy Objects")]
    public Camera mainCamera;
    public PilePlacer foodPile;
    public FoodPileUI foodUI;
    public InControl.InControlManager inControlManager;

    [Header("Objects for next scene")]
    public EndDialogueManager endDialogueManager;
    public VignetteSetter vignetteSetter;
    public FoodPileUISoundPlayer foodSoundPlayer;

    private bool isBlackingOut = false;
    private bool isFadingIn = false;

    public delegate void SceneEndTimeAction();
    public event SceneEndTimeAction OnScreenCompletelyBlack;
    public event SceneEndTimeAction OnScreenCompletelyVisible;

    

    private float fullAlphaApproximation = 0.991f;
    private float emptyAlphaApproximation = 0.009f;

    private float exponentializer = 0f;

    private void OnEnable()
    {
        endCutscene.OnSequenceStarted += StartScreenBlackoutAfterCorrectSequence;
        SceneManager.sceneLoaded += LoadNewScene;
    }

    private void OnDisable()
    {
        endCutscene.OnSequenceStarted -= StartScreenBlackoutAfterCorrectSequence;
        SceneManager.sceneLoaded -= LoadNewScene;
    }

    private void Update()
    {
        UpdateBlackout();
    }

    private void StartScreenBlackoutAfterCorrectSequence(int sequenceOrder)
    {
        if(sequenceOrder == startSwitchAtSequenceOrder)
        {
            BlackOut();
        }
    }

    public void BlackOut()
    {
        blackoutImage.color = colorBegin;
        isBlackingOut = true;
        isFadingIn = false;
        currentColor = colorFinal;
        exponentializer = 0;
    }

    private void FadeIn()
    {
        blackoutImage.color = colorFinal;
        isFadingIn = true;
        isBlackingOut = false;
        currentColor = colorBegin;
        exponentializer = 0;
    }

    private void UpdateBlackout()
    {
        if (isBlackingOut && blackoutImage != null)
        {
            exponentializer += Time.deltaTime * 2;
            blackoutImage.color = Color.Lerp(blackoutImage.color, currentColor, blackoutSpeed * Time.deltaTime * exponentializer);

            if (blackoutImage.color.a >= fullAlphaApproximation)
            {
                isBlackingOut = false;
                blackoutImage.color = colorFinal;
                if (OnScreenCompletelyBlack != null)
                {
                    OnScreenCompletelyBlack();
                }
                ChangeScene();
            }
        }
        else if (isFadingIn && blackoutImage != null)
        {
            exponentializer += Time.deltaTime * 2;
            blackoutImage.color = Color.Lerp(blackoutImage.color, currentColor, blackoutSpeed * Time.deltaTime * exponentializer);
            if(blackoutImage.color.a <= emptyAlphaApproximation)
            {
                isFadingIn = false;
                blackoutImage.color = colorBegin;
                if(OnScreenCompletelyVisible != null)
                {
                    OnScreenCompletelyVisible();
                }

                /*
                if(setupper != null)
                {
                    setupper.BeginCameraMovement();
                }
                */
            }
        }
    }

    private void ChangeScene()
    {
        DontDestroyParent(this.gameObject);
        DontDestroyParent(mainCamera.gameObject);
        DontDestroyOnLoad(foodPile.gameObject);
        DontDestroyOnLoad(inControlManager.gameObject);
        //SceneManager.LoadScene(sceneToSwitchTo);
        SceneManager.LoadSceneAsync(sceneToSwitchTo);

    }

    private void DontDestroyParent(GameObject child)
    {
        Transform parentTransform = child.transform;

        while(parentTransform.parent != null)
        {
            parentTransform = parentTransform.parent;
        }

        GameObject.DontDestroyOnLoad(parentTransform.gameObject);
    }

    private void LoadNewScene(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == sceneToSwitchTo)
        {
            //print("New scene setup start");
            setupper = FindObjectOfType<FinalSceneSetup>();

            if (setupper != null)
            {
                //print("Successful");
                //Set Pile Location
                Transform pileLocation = setupper.pileLocation;
                Transform raftLocation = setupper.raft.transform;

                foodPile.transform.position = pileLocation.position;
                foodPile.transform.parent = raftLocation;

                //Set camera location
                CameraMover cMover = mainCamera.GetComponent<CameraMover>();
                setupper.SetupCameraMover(cMover);

                setupper.SetEnding(foodUI.GetCurrentEnding, vignetteSetter, endDialogueManager, foodSoundPlayer, promptImage, promptImageChanger, blackOuter, this);

                //print("Setting false ending number for testing");
                //setupper.SetEnding(2, vignetteSetter, endDialogueManager, foodSoundPlayer);

                cMover.StopCameraMovement();
                //Set cam location
                
                mainCamera.transform.position = new Vector3(setupper.cameraStart.position.x, setupper.cameraStart.position.y, mainCamera.transform.position.z);
            }
            FadeIn();
            if (setupper != null)
            {
                setupper.BeginCameraMovement();
            }
        }
    }

}
