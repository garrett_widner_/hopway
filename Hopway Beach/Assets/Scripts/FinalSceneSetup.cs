﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FinalSceneSetup : MonoBehaviour
{
    public Transform pileLocation;
    public GameObject raft;

    public Transform cameraStart;
    public Transform cameraMidpoint;
    public Transform cameraEnd;

    public float hangTimeOnCharacters = 4f;
    public float timeAfterFinalMoveStartsToShowEndScreen = 4f;
    public float moveSpeed = 1f;

    private int endingNumber;
    private EndDialogueManager endDialogueManager;
    private VignetteSetter vignetteSetter;
    public Transform vignetteSpawn;
    private FoodPileUISoundPlayer foodSoundPlayer;
    public FinalPrompt finalPrompt;
    public string startSceneName;
    public Transform holeSpawner;

    private PromptImageChanger promptImageChanger;
    private ScreenBlackout blackOuter;
    private EndSceneSwitcher endSceneSwitcher;

    private int currentUI = 0;
    private bool uiCanAdvance = false;
    private float timeBetweenOldUIFadeOutAndNewUIFadeIn = 1.8f;
    private float timeBetweenNewUIFadeInAndPromptAppearance = 3f;
    private bool hasSeenCharacters = false;

    private bool canMoveToNextScene = true;

    private CameraMover camMover;

    private PlayerActions playerActions;

    private void Start()
    {
        playerActions = PlayerActions.CreateWithDefaultBindings();
    }

    public void SetupCameraMover(CameraMover cMover)
    {
        camMover = cMover;
        camMover.OnCameraFinishedMoving += BeginFinalMovementSequence;
    }

    public void SetEnding(int ending, VignetteSetter vs, EndDialogueManager edm, FoodPileUISoundPlayer fpsp, Image promptImage, PromptImageChanger imageChanger, ScreenBlackout blackOut, EndSceneSwitcher ess)
    {
        endingNumber = ending;
        vignetteSetter = vs;
        vs.vignetteSpawnTransform = vignetteSpawn;
        endDialogueManager = edm;
        foodSoundPlayer = fpsp;
        finalPrompt.promptImage = promptImage;
        promptImageChanger = imageChanger;

        blackOuter = blackOut;
        blackOuter.OnScreenCompletelyBlack += MoveToStartSceneAfterBlackout;
        endSceneSwitcher = ess;
    }

    private void OnDisable()
    {
        if(camMover != null)
        {
            camMover.OnCameraFinishedMoving -= BeginFinalMovementSequence;
        }

        if(blackOuter != null)
        {
            blackOuter.OnScreenCompletelyBlack -= MoveToStartSceneAfterBlackout;
        }
    }

    public void BeginCameraMovement()
    {
        camMover.MoveCameraTo(cameraMidpoint.position, moveSpeed);
    }

    public void BeginFinalMovementSequence()
    {
        if(!hasSeenCharacters)
        {
            Invoke("FinalMovement", hangTimeOnCharacters);
            hasSeenCharacters = true;
        }

        holeSpawner.SetParent(camMover.transform);
    }

    public void FinalMovement()
    {
        camMover.MoveCameraTo(cameraEnd.position, moveSpeed);
        Invoke("ShowEndScreens", timeAfterFinalMoveStartsToShowEndScreen);
    }

    public void ShowEndScreens()
    {
        ShowVignetteScreen();
    }

    private void Update()
    {
        if (playerActions.Pickup.WasPressed)
        {
            if (uiCanAdvance)
            {
                CloseCurrentAndAdvanceToNextUI();
            }
        }
    }

    //uicanadvance is set to true when the previous ui is fully visible (estimated through timer)
    //if uicanadvance and pickup button is pressed, start closing current ui and trigger next ui to start appearing.
    private void CloseCurrentAndAdvanceToNextUI()
    {
        finalPrompt.HidePrompt();
        StopAdvanceToNextUI();

        if(currentUI == 0)
        {
            HideVignetteScreen();
        }
        else if(currentUI == 1)
        {
            HideSoundAttributions();

        }
        else if(currentUI == 2)
        {
            HideCredits();
            Blackout();
        }

        currentUI++;

        Invoke("AdvanceToNextUI", timeBetweenOldUIFadeOutAndNewUIFadeIn);
    }

    private void AdvanceToNextUI()
    {
        if (currentUI == 1)
        {
            ShowSoundAttributions();
            Invoke("ShowFinalPromptAndAllowUIAdvance", timeBetweenNewUIFadeInAndPromptAppearance);
        }
        else if (currentUI == 2)
        {
            ShowCredits();
            Invoke("ShowFinalPromptAndAllowUIAdvance", timeBetweenNewUIFadeInAndPromptAppearance);
        }
    }

    private void ShowVignetteScreen()
    {
        endDialogueManager.SetText(endingNumber);
        vignetteSetter.SpawnCorrectVignette(endingNumber, camMover.GetComponent<Camera>());

        ShowVignette();
        foodSoundPlayer.PlayPop();

        EndingSaver.Instance.UnlockEnding(endingNumber);

        Invoke("ShowTextBox", 1.5f);
        Invoke("ShowCaption", 10f);
        Invoke("ShowFinalPromptAndAllowUIAdvance", 13.5f);
    }

    private void HideVignetteScreen()
    {
        endDialogueManager.CloseTextBox();
        endDialogueManager.CloseCaptionBox();
        DeleteVignette();
        foodSoundPlayer.PlayPop();
    }

    private void ShowCredits()
    {
        endDialogueManager.OpenCredits();
    }

    private void HideCredits()
    {
        endDialogueManager.CloseCredits();
    }

    private void ShowSoundAttributions()
    {
        endDialogueManager.OpenSoundAttributions();
    }

    private void HideSoundAttributions()
    {
        endDialogueManager.CloseSoundAttributions();
    }

    private void ShowVignette()
    {
        vignetteSetter.PlaceVignette();
    }

    private void DeleteVignette()
    {
        vignetteSetter.DeleteVignette();
    }

    private void ShowTextBox()
    {
        endDialogueManager.OpenTextBox();
    }

    private void ShowCaption()
    {
        endDialogueManager.OpenCaptionBox();
    }

    private void ShowFinalPromptAndAllowUIAdvance()
    {
        promptImageChanger.UsePressPrompt();
        finalPrompt.ShowPrompt();
        AllowAdvanceToNextUI();
    }

    private void AllowAdvanceToNextUI()
    {
        uiCanAdvance = true;
    }

    private void StopAdvanceToNextUI()
    {
        uiCanAdvance = false;
    }

    private void Blackout()
    {
        if(canMoveToNextScene)
        {
            if (blackOuter != null)
            {
                blackOuter.BlackOut();
                canMoveToNextScene = false;
            }
        }
    }

    private void MoveToStartSceneAfterBlackout()
    {
        finalPrompt.HidePrompt();
        DestroyAllDontDestroyObjects();
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(startSceneName);
    }

    private void DestroyAllDontDestroyObjects()
    {
        //HUDCanvas
        DestroyParent(endDialogueManager.gameObject);

        //Player
        DestroyParent(FindObjectOfType<PlayerController>().gameObject);

        //InControl
        DestroyParent(FindObjectOfType<InControl.InControlManager>().gameObject);

        //CutsceneObjects
        DestroyParent(endSceneSwitcher.gameObject);
    }

    private void DestroyParent(GameObject child)
    {
        Transform parentTransform = child.transform;

        while (parentTransform.parent != null)
        {
            parentTransform = parentTransform.parent;
        }

        DestroyImmediate(parentTransform.gameObject);
    }



}
