﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PromptImageChanger : MonoBehaviour
{
    public Image promptImage;
    [Space]
    public Sprite controllerPickup;
    public Sprite controllerPause;
    public Sprite controllerPress;
    public Sprite controllerSkip;
    public Sprite keyboardPickup;
    public Sprite keyboardPause;
    public Sprite keyboardPress;
    public Sprite keyboardSkip;

    private PlayerActions playerActions;

    private bool isCurrentlyUsingController = false;
    private bool isOnStartPrompt = false;
    private bool isOnSkipPrompt = false;
    private bool isOnPressPrompt = false;

    private void Start()
    {
        playerActions = PlayerActions.CreateWithDefaultBindings();
        CheckForInputTypeChangeAndUpdateImage();
    }

    public void UseStartPrompt()
    {
        isOnStartPrompt = true;
        isOnSkipPrompt = false;
        isOnPressPrompt = false;
        UpdatePromptImage();
    }

    public void UsePickupPrompt()
    {
        isOnStartPrompt = false;
        isOnSkipPrompt = false;
        isOnPressPrompt = false;
        UpdatePromptImage();
    }

    public void UseSkipPrompt()
    {
        isOnStartPrompt = false;
        isOnSkipPrompt = true;
        isOnPressPrompt = false;
        UpdatePromptImage();
    }

    public void UsePressPrompt()
    {
        isOnStartPrompt = false;
        isOnSkipPrompt = false;
        isOnPressPrompt = true;
        UpdatePromptImage();
    }

    private void CheckForInputTypeChangeAndUpdateImage()
    {
        bool wasUsingController = isCurrentlyUsingController;
        isCurrentlyUsingController = (playerActions.LastInputType == InControl.BindingSourceType.DeviceBindingSource);

        if(isCurrentlyUsingController != wasUsingController)
        {
            UpdatePromptImage();
        }
    }

    private void UpdatePromptImage()
    {
        if (isCurrentlyUsingController)
        {
            UpdateControllerPromptImage();
        }
        else if (!isCurrentlyUsingController)
        {
            UpdateKeyboardPromptImage();
        }
    }

    private void UpdateControllerPromptImage()
    {
        if (isOnStartPrompt)
        {
            promptImage.sprite = controllerPause;
        }
        else if(isOnSkipPrompt)
        {
            promptImage.sprite = controllerSkip;
        }
        else if(isOnPressPrompt)
        {
            promptImage.sprite = controllerPress;
        }
        else
        {
            promptImage.sprite = controllerPickup;
        }
    }

    private void UpdateKeyboardPromptImage()
    {
        if (isOnStartPrompt)
        {
            promptImage.sprite = keyboardPause;
        }
        else if(isOnSkipPrompt)
        {
            promptImage.sprite = keyboardSkip;
        }
        else if(isOnPressPrompt)
        {
            promptImage.sprite = keyboardPress;
        }
        else
        {
            promptImage.sprite = keyboardPickup;
        }
    }

    private void Update()
    {
        CheckForInputTypeChangeAndUpdateImage();
    }
}
