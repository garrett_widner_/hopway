﻿using UnityEngine;
using System.Collections;

public class StatusBarEventTester : MonoBehaviour 
{
    public StatusBar statBar;

    private void OnEnable()
    {
        statBar.OnStatLevelHadTimedIncreaseThisFrame += IncrementalIncrease;
        statBar.OnStatLevelHadTimedDecreaseThisFrame += IncrementalDecrease;
        statBar.OnStatLevelStartedTimedDecrease += StartedDecrease;
        statBar.OnStatLevelStartedTimedIncrease += StartedIncrease;
        statBar.OnStatLevelHitMax += HitMax;
        statBar.OnStatLevelHitZero += HitZero;
    }

    private void OnDisable()
    {
        statBar.OnStatLevelHadTimedIncreaseThisFrame -= IncrementalIncrease;
        statBar.OnStatLevelHadTimedDecreaseThisFrame -= IncrementalDecrease;
        statBar.OnStatLevelStartedTimedDecrease -= StartedDecrease;
        statBar.OnStatLevelStartedTimedIncrease -= StartedIncrease;
        statBar.OnStatLevelHitMax -= HitMax;
        statBar.OnStatLevelHitZero -= HitZero;
    }

    private void StartedDecrease(float amount)
    {
        print("------------------------------------Started a decrease of " + amount + ".");
    }

    private void StartedIncrease(float amount)
    {
        print("--------------------------Started an increase of " + amount + ".");
    }

    private void IncrementalIncrease(float amount)
    {
        print("Increased by " + amount + " this frame");
    }

    private void IncrementalDecrease(float amount)
    {
        print("Decreased by " + amount + " this frame");
    }

    private void HitZero()
    {
        print("000000000000000000000000000000000000 Hit Zero");
    }

    private void HitMax()
    {
        print("9999999999999999999999999999999999999999 Hit Max");
    }
}
