﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneSkipper : MonoBehaviour
{
    public CutsceneSequencer startCutscene;
    public CutsceneSequencer endCutscene;

    public DialogueManager dialogueManager;

    public Animator natAnimator;
    public string skipTrigger;
    public ScreenBlackout screenBlackout;
    public EndSceneSwitcher endSceneSwitcher;
    public TextPromptDisplayer textPrompt;

    private bool cutsceneHasBeenSkipped = false;
    public bool CutsceneHasBeenSkipped
    {
        get
        {
            return cutsceneHasBeenSkipped;
        }
    }

    //public Animator iliaAnimator;

    private PlayerActions playerActions;

    private float secondsPressed;
    public float minSecondsToTriggerSkip = 2f;

    private void OnEnable()
    {
        screenBlackout.OnScreenCompletelyBlack += FadeInOnStartCutscene;
        endSceneSwitcher.OnScreenCompletelyBlack += CutsceneWasSkipped;
        endSceneSwitcher.OnScreenCompletelyBlack += endCutscene.StopCutscenePart2;

        startCutscene.OnCutsceneEnd += CutsceneWasSkipped;
        //endCutscene.OnCutsceneEnd += CutsceneWasSkipped;
    }

    private void OnDisable()
    {
        screenBlackout.OnScreenCompletelyBlack -= FadeInOnStartCutscene;
        endSceneSwitcher.OnScreenCompletelyBlack -= CutsceneWasSkipped;
        endSceneSwitcher.OnScreenCompletelyBlack += endCutscene.StopCutscenePart2;

        startCutscene.OnCutsceneEnd -= CutsceneWasSkipped;
        //endCutscene.OnCutsceneEnd -= CutsceneWasSkipped;
    }

    private bool IsCutscenePlaying
    {
        get
        {
            return (startCutscene.IsCutscenePlaying || endCutscene.IsCutscenePlaying);
        }
    }

    private void Start()
    {
        playerActions = PlayerActions.CreateWithDefaultBindings();
    }

    private void Update()
    {
        if(IsCutscenePlaying && !cutsceneHasBeenSkipped)
        {
            if(playerActions.Pickup.IsPressed)
            {
                secondsPressed += Time.deltaTime;
            }
            else if(playerActions.Pickup.WasReleased)
            {
                secondsPressed = 0;
            }
            if(secondsPressed >= minSecondsToTriggerSkip)
            {
                SkipCutscene();
                secondsPressed = 0;
            }
        }
    }

    private void SkipCutscene()
    {
        cutsceneHasBeenSkipped = true;
        dialogueManager.StopTyping();

        //print("Cutscene being skipped");
        if (startCutscene.IsCutscenePlaying)
        {
            screenBlackout.BlackOut();
            startCutscene.StopCutscenePart1();
            textPrompt.StopDisplay();
        }

        if (endCutscene.IsCutscenePlaying)
        {
            endSceneSwitcher.BlackOut();
            endCutscene.StopCutscenePart1();
            textPrompt.StopDisplay();

        }
    }

    private void FadeInOnStartCutscene()
    {
        if(startCutscene.IsCutscenePlaying)
        {
            //Change everything to cutscene end
            natAnimator.SetTrigger(skipTrigger);
            startCutscene.SkipCutscene();

            screenBlackout.FadeIn();
        }
    }

    private void CutsceneWasSkipped()
    {
        cutsceneHasBeenSkipped = false;
    }



}
