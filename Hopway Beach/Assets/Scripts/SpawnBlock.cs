﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBlock : MonoBehaviour
{
    public GameObject[] spawnablePrefabs;
    public BoxCollider2D spawnBox;

    public bool spawnsOnTimer = true;
    public float minSpawnTime;
    public float maxSpawnTime;


    private void Start()
    {
        if(spawnsOnTimer)
        {
            SpawnRandomPrefabContinually();
        }
    }

    private void SpawnRandomPrefab()
    {
        int prefabToSpawn = Random.Range(0, spawnablePrefabs.Length - 1);
        Vector2 randomSpawnLocation = new Vector2(Random.Range(spawnBox.bounds.min.x, spawnBox.bounds.max.x),
                                                  Random.Range(spawnBox.bounds.min.y, spawnBox.bounds.max.y));
        Instantiate(spawnablePrefabs[prefabToSpawn], randomSpawnLocation, Quaternion.identity);
    }

    private void SpawnRandomPrefabContinually()
    {
        SpawnRandomPrefab();
        Invoke("SpawnRandomPrefabContinually", Random.Range(minSpawnTime, maxSpawnTime));
    }

}
