﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StartButtonPrompt : ButtonPressPrompt
{
    public CutsceneSequencer endCutscene;
    public PromptImageChanger promptImageChanger;

    private PlayerActions playerActions;

    private float timeAfterTriggerToStartPrompt = 20f;

    protected override void Start()
    {
        base.Start();
        playerActions = PlayerActions.CreateWithDefaultBindings();
    }

    private void OnEnable()
    {
        endCutscene.OnCutsceneStart += EndPrompt;
    }

    private void OnDisable()
    {
        endCutscene.OnCutsceneStart -= EndPrompt;

    }

    protected override void CheckForPromptStart()
    {

    }

    protected override void CheckForPromptEnd()
    {
        if(promptIsPlaying)
        {
            //print("Prompt is supposedly playing");
            if (playerActions.Pause.WasPressed)
            {
                EndPrompt();
            }
        }
   
    }

    protected override void CheckForAnimationPause()
    {
    }

    protected override void CheckForAnimationResume()
    {
    }

    public void SchedulePromptStart()
    {
        Invoke("BeginPrompt", timeAfterTriggerToStartPrompt);
    }

    private void BeginPrompt()
    {
        promptImageChanger.UseStartPrompt();
        StartPrompt();
    }

}
