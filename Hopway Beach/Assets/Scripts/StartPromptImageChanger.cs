﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartPromptImageChanger : MonoBehaviour
{
    public Image promptImage;

    public Sprite controllerPrompt;
    public Sprite keyboardPrompt;

    private PlayerActions playerActions;

    private bool isCurrentlyUsingController = false;

    private void Start()
    {
        playerActions = PlayerActions.CreateWithDefaultBindings();
        CheckForInputTypeChangeAndUpdateImage();
    }

    private void Update()
    {
        CheckForInputTypeChangeAndUpdateImage();
    }

    private void CheckForInputTypeChangeAndUpdateImage()
    {
        bool wasUsingController = isCurrentlyUsingController;
        isCurrentlyUsingController = (playerActions.LastInputType == InControl.BindingSourceType.DeviceBindingSource);

        if (isCurrentlyUsingController != wasUsingController)
        {
            UpdatePromptImage();
        }
    }

    private void UpdatePromptImage()
    {
        if (isCurrentlyUsingController)
        {
            promptImage.sprite = controllerPrompt;
        }
        else if (!isCurrentlyUsingController)
        {
            promptImage.sprite = keyboardPrompt;
        }
    }
}
