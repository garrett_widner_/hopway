﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogueManager : MonoBehaviour
{
    public Text nameText;
    public Text dialogueText;

    public Animator animator;

    private Queue<Statement> statements = new Queue<Statement>();

    private bool canGoToNextStatement = true;
    public float timeBeforeAllowingNextStatement = 0.5f;
    public float timeBetweenDialogueBoxAndTextAppearance = 0.2f;
    public float timeBetweenNameAndTextAppearance = 0.2f;

    private bool dialogueIsOpen = false;

    private PlayerActions playerActions;

    public delegate void DialogueAction(string s);
    public event DialogueAction OnNewStatement;

    public delegate void BoxImageAction();
    public event BoxImageAction OnBoxFullyVisible;
    public event BoxImageAction OnBoxInvisible;

    public delegate void DialogueStartAction();
    public event DialogueStartAction OnDialogueStart;
    public event DialogueStartAction OnDialogueEnd;

    public bool DialogueIsPlaying
    {
        get
        {
            return dialogueIsOpen;
        }
    }

    public Queue<Statement> StatementsQueue
    {
        get
        {
            return statements;
        }
    }

    private Statement upcomingStatement;

    private void Start()
    {
        statements = new Queue<Statement>();
        ClearAllText();
        playerActions = PlayerActions.CreateWithDefaultBindings();
    }

    private void Update()
    {
        if(playerActions.Pickup.WasPressed)
        {
            if(dialogueIsOpen)
            {
                DisplayNextStatement();
            }
        }
    }

    private void ClearAllText()
    {
        nameText.text = "";
        dialogueText.text = "";
    }

    public void DialogueBoxIsInvisible()
    {
        if(OnBoxInvisible != null)
        {
            OnBoxInvisible();
        }
    }

    public void DialogueBoxIsVisible()
    {
        Invoke("DisplayNextStatement", timeBetweenDialogueBoxAndTextAppearance);
    }

    public void StartDialogue(Dialogue dialogue)
    {
        dialogueIsOpen = true;
        animator.SetBool("IsOpen", true);
        statements.Clear();

        if(OnDialogueStart != null)
        {
            OnDialogueStart();
        }

        foreach (Statement statement in dialogue.statements)
        {
            statements.Enqueue(statement);
        }
    }

    public void DisplayNextStatement()
    {
        DisplayNextStatement(timeBetweenNameAndTextAppearance);
    }

    public void DisplayNextStatement(float timeNameVisibleBeforeText)
    {
        if(canGoToNextStatement)
        {
            StopAllCoroutines();

            canGoToNextStatement = false;
            Invoke("AllowGoingToNextStatement", timeBeforeAllowingNextStatement);

            if (statements.Count == 0)
            {
                EndDialogue();
                return;
            }

            upcomingStatement = statements.Dequeue();

            StartCoroutine(TypeName(upcomingStatement.characterName));
            dialogueText.text = "";
            Invoke("TypeUpcomingSentence", timeNameVisibleBeforeText);
        }
    }

    public void StopTyping()
    {
        StopAllCoroutines();
    }

    private void TypeUpcomingSentence()
    {
        StartCoroutine(TypeSentence(upcomingStatement));
    }

    IEnumerator TypeSentence(Statement statement)
    {
        dialogueText.text = "";

        foreach (char letter in statement.text.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }

        if(OnBoxFullyVisible != null)
        {
            OnBoxFullyVisible();
        }
    }

    IEnumerator TypeName(string name)
    {
        if(nameText.text != name)
        {
            nameText.text = "";

            foreach (char letter in name.ToCharArray())
            {
                nameText.text += letter;
                yield return null;
            }
        }
    }

    public void EndDialogue()
    {
        dialogueIsOpen = false;
        ClearAllText();
        Invoke("ClosingAnimation", timeBetweenDialogueBoxAndTextAppearance);
        Invoke("EndDialogueEvent", timeBetweenDialogueBoxAndTextAppearance);

    }

    private void ClosingAnimation()
    {
        animator.SetBool("IsOpen", false);
    }

    private void EndDialogueEvent()
    {
        if (OnDialogueEnd != null)
        {
            OnDialogueEnd();
        }
    }

    private void AllowGoingToNextStatement()
    {
        canGoToNextStatement = true;
        
    }
}
