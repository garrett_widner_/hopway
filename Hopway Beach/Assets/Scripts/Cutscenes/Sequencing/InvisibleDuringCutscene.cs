﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvisibleDuringCutscene : MonoBehaviour
{
    public Image[] foodBarImages;
    public CutsceneSequencer cutsceneSequencer;
    public PauseManager pauseManager;

    private void OnEnable()
    {
        //cutsceneSequencer.OnCutsceneStart += MakeBarInvisible;
        cutsceneSequencer.OnCutsceneEnd += MakeBarVisible;
        pauseManager.OnPaused += MakeBarInvisible;
        pauseManager.OnUnpause += MakeBarVisible;
    }

    private void OnDisable()
    {
        //cutsceneSequencer.OnCutsceneStart += MakeBarInvisible;
        cutsceneSequencer.OnCutsceneEnd += MakeBarVisible;
        pauseManager.OnPaused -= MakeBarInvisible;
        pauseManager.OnUnpause -= MakeBarVisible;
    }

    private void MakeBarVisible()
    {
        foreach(Image i in foodBarImages)
        {
            i.enabled = true;
        }
    }

    private void MakeBarInvisible()
    {
        foreach (Image i in foodBarImages)
        {
            i.enabled = false;
        }
    }

}
