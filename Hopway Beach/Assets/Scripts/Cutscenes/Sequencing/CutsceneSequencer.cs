﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CutsceneSequencer : MonoBehaviour
{
    public bool playCutscene = false;
    public bool isStartCutscene = false;
    public GroundMover playerGroundMover;
    private bool cutsceneIsPlaying = false;
    private bool cutsceneHasPlayed = false;
    public DialogueManager dialogueManager;
    public float waitAfterLastSequence = 2f;

    private bool cutsceneShouldStop = false;

    private PlayerActions playerActions;

    [Header("Camera Movements")]
    public CameraMovementSequence[] cameraMovements;
    [Header("Dialogues")]
    public DialogueSequence[] dialogues;
    [Header("Animations")]
    public AnimationSequence[] animations;

    private List<Sequence> sequences;

    public delegate void SequenceAction(int order);
    public event SequenceAction OnSequenceStarted;

    public bool IsCutscenePlaying
    {
        get
        {
            return cutsceneIsPlaying;
        }
    }

    public delegate void CutsceneAction();
    public event CutsceneAction OnCutsceneEnd;
    public event CutsceneAction OnCutsceneStart;

    private void Start()
    {
        playerActions = PlayerActions.CreateWithDefaultBindings();
        
        
    }

    private void Update()
    {
        if (playerActions.Pickup.WasPressed && !cutsceneHasPlayed && isStartCutscene)
        {
            cutsceneHasPlayed = true;

            if (playCutscene)
            {
                CreateSequenceList();
                cutsceneIsPlaying = true;
                StartCutscene();
            }
            else
            {
                //For skipping starting cutscene
                SkipCutscene();
            }
        }
    }

    public void SkipCutscene()
    {
        cutsceneShouldStop = true;
        Sequence seq = cameraMovements[cameraMovements.Length - 1];
        PlayCameraMovementSequence(seq as CameraMovementSequence);

        EndCutscene();

        if (OnCutsceneEnd != null)
        {
            OnCutsceneEnd();
        }
    }

    public void BeginCutscene()
    {
        cutsceneHasPlayed = true;
        CreateSequenceList();
        cutsceneIsPlaying = true;

        StartCutscene();
    }

    private void CreateSequenceList()
    {
        sequences = new List<Sequence>();
        sequences.AddRange(cameraMovements);
        sequences.AddRange(dialogues);
        sequences.AddRange(animations);

        sequences.Sort((x, y) => x.order.CompareTo(y.order));

        /*
        string strang = "";
        foreach (Sequence seq in sequences)
        {
            strang = strang + seq.order + " ";
        }
        print(strang);

        sequences.Sort((x, y) => x.order.CompareTo(y.order));

        print("________________");

        strang = "";
        foreach(Sequence seq in sequences)
        {
            strang = strang + seq.order + " ";
        }
        print(strang);
        */
    }

    public void StartCutscene()
    {
        //print("Cutscene named " + gameObject.name + " started.");

        if (OnCutsceneStart != null)
        {
            OnCutsceneStart();
        }
        StartCoroutine(PlaySequencesInTurn());
    }

    private void EndDialogueSequence()
    {

    }

    private IEnumerator PlaySequencesInTurn()
    {
        int counter = 0;
        foreach (Sequence seq in sequences)
        {
            //print("Playing sequence " + (counter + 1));

            /*
            //Stopping Before Button Press
            bool canContinue = true;
            if (seq.waitForButtonPressBefore)
            {
                canContinue = false;
            }
            while (!canContinue)
            {
                if (Input.GetKeyDown(KeyCode.J))
                {
                    canContinue = true;
                    //print("Okaynow");
                }
                yield return null;
            }
            */

            if(cutsceneShouldStop)
            {
                StopCutscenePart2();
                yield return null;

            }

            //Waiting
            if (seq.waitBeforehand >= 0.1f)
            {
                yield return new WaitForSeconds(seq.waitBeforehand);
            }

            //print("Sequence start");

            if(OnSequenceStarted != null)
            {
                OnSequenceStarted(seq.order);
            }
            
            System.Type sequenceType = seq.GetType(); 
            if(sequenceType == typeof(CameraMovementSequence))
            {
                PlayCameraMovementSequence(seq as CameraMovementSequence);
            }
            else if (sequenceType == typeof(DialogueSequence))
            {
                yield return StartCoroutine(PlayDialogueSequence(seq as DialogueSequence));
            }
            else if (sequenceType == typeof(AnimationSequence))
            {
                PlayAnimationSequence(seq as AnimationSequence);
            }

            //print(counter + " time: " + Time.time);
            counter++;
        }

        if (waitAfterLastSequence >= 0.1f)
        {
            yield return new WaitForSeconds(waitAfterLastSequence);
        }

        EndCutscene();
        yield return null;
    }

    public void StopCutscenePart1()
    {
        dialogueManager.EndDialogue();
        StopAllCoroutines();
    }

    public void StopCutscenePart2()
    {
        //print("being told to stop cutscene");
        StopAllCoroutines();
        dialogueManager.EndDialogue();
        EndCutscene();
    }

    private void PlayCameraMovementSequence(CameraMovementSequence cameraSequence)
    {
        cameraSequence.cameraMover.MoveCameraTo(cameraSequence.moveLocation, cameraSequence.speed, cameraSequence.locksToLocationWhenFinished);
    }

    private IEnumerator PlayDialogueSequence(DialogueSequence dialogueSequence)
    {
        dialogueManager.StartDialogue(dialogueSequence.dialogue);

        do
        {
            yield return null;
        } while (dialogueManager.DialogueIsPlaying);
    }

    private void PlayAnimationSequence(AnimationSequence animationSequence)
    {
        animationSequence.animator.SetTrigger(animationSequence.triggerName);
    }

    private void EndCutscene()
    {
        //print("Cutscene End");
        cutsceneIsPlaying = false;
        //print("Cutscene named " + gameObject.name + " ended.");
        if(OnCutsceneEnd != null)
        {
            OnCutsceneEnd();
        }
        //playerGroundMover.AllowMovement();
        //playerGroundMover.AllowTurning();
    }

}
