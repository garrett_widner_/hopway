﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogueSequence : Sequence
{
    public Dialogue dialogue;
}
