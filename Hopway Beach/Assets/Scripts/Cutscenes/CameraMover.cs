﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    private bool isLockedToObject;
    private Transform lockedObject;

    public Vector2 lockOffset;

    public bool lockToTransformOnStart = true;
    public Transform startTransformLock;

    //Lerping
    public float lerpSpeed = 1f;
    private Vector2 startPoint;
    private Vector2 endPoint;
    private Transform transformEndpoint;
    private float journeyLength;
    private float startTime;

    private bool isLerping = false;
    private bool lockToTransform = false;

    public delegate void CameraMovementAction();
    public event CameraMovementAction OnCameraFinishedMoving;

    public void LockToObject(Transform objectTransform)
    {
        isLockedToObject = true;
        transform.parent = objectTransform;
        transform.localPosition = new Vector3(lockOffset.x, lockOffset.y,transform.localPosition.z);
    }

    public void UnlockFromObject()
    {
        isLockedToObject = false;
        transform.parent = null;
    }

    private void Start()
    {
        if(lockToTransformOnStart && startTransformLock != null)
        {
            LockToObject(startTransformLock);
        }
    }

    private void Update()
    {
        if(isLerping)
        {
            if(transformEndpoint != null)
            {
                journeyLength = Vector2.Distance(startPoint, transformEndpoint.transform.position);
                endPoint = transformEndpoint.position;

                if (journeyLength == 0.0f)
                {
                    return;
                }
            }
            float distCovered = (Time.time - startTime) * lerpSpeed;
            float fracJourney = distCovered / journeyLength;
            //print(fracJourney + " = " + distCovered.ToString("F4") + " / " + journeyLength.ToString("F4"));
            Vector3 newPosition = Vector3.Lerp(startPoint, endPoint, fracJourney);
            transform.position = new Vector3(newPosition.x, newPosition.y, transform.position.z);

            if(fracJourney > 1)
            {
                isLerping = false;
                if(lockToTransform && transformEndpoint != null)
                {
                    LockToObject(transformEndpoint);
                }
                if(OnCameraFinishedMoving != null)
                {
                    OnCameraFinishedMoving();
                }
            }
        }
    }

    public void MoveCameraTo(Vector2 location, float speed)
    {
        isLerping = true;
        lerpSpeed = speed;
        transformEndpoint = null;
        startTime = Time.time;
        //print("Start" + startTime.ToString("F4"));
        startPoint = transform.position;
        //print("Start " + startPoint.ToString("F4"));
        endPoint = location;
        //print("End " + endPoint.ToString("F4"));

        journeyLength = Vector2.Distance(startPoint, endPoint);
        //print("Journey Length = " + journeyLength.ToString("F4"));
    }

    public void StopCameraMovement()
    {
        isLerping = false;
    }
    
    public void MoveCameraTo(Transform location, float speed, bool lockToTransformOnLerpEnd = false)
    {
        MoveCameraTo(location.position, speed);
        transformEndpoint = location;
        lockToTransform = lockToTransformOnLerpEnd;
    }
    


}
