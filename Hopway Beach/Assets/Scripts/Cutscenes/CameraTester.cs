﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTester : MonoBehaviour
{
    public CameraMover cameraMover;
    public Transform characterTransform;

    

    private void Start()
    {
        cameraMover.LockToObject(characterTransform);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.L))
        {
            cameraMover.LockToObject(characterTransform);

        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            cameraMover.UnlockFromObject();
        }
        if(Input.GetKeyDown(KeyCode.B))
        {
            cameraMover.MoveCameraTo(characterTransform, 1f, true);
        }
    }
}
