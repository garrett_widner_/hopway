﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostCutsceneMovementStarter : MonoBehaviour
{
    public CutsceneSequencer cutsceneSequencer;
    public SpriteRenderer playerControllerSprite;
    public SpriteRenderer playerCutsceneSprite;
    public GroundMover playerGroundMover;
    public PauseManager pauseManager;

    private void OnEnable()
    {
        //cutsceneSequencer.OnCutsceneStart += DisableCharacterControllerAndMovement;
        cutsceneSequencer.OnCutsceneEnd += EnableCharacterControllerAndMovement;
        //pauseManager.OnFirstPauseEnd += EnableMovement;
    }

    private void OnDisable()
    {
        //cutsceneSequencer.OnCutsceneStart -= DisableCharacterControllerAndMovement;
        cutsceneSequencer.OnCutsceneEnd -= EnableCharacterControllerAndMovement;
        //pauseManager.OnFirstPauseEnd -= EnableMovement;

    }

    private void Start()
    {
        DisableCharacterControllerAndMovement();
    }

    public void DisableCharacterControllerAndMovement()
    {
        //print("Disabled Character Controller + Movement");
        DisableCharacterController();
        DisableMovement();
    }

    public void DisableCharacterController()
    {
        //print("Disabled Character Controller");
        playerControllerSprite.enabled = false;
        playerCutsceneSprite.enabled = true;
    }

    public void DisableMovement()
    {
        //print("Disabled Movement");
        playerGroundMover.DisallowMovement();
        playerGroundMover.DisallowTurning();
    }

    public void EnableCharacterControllerAndMovement()
    {
        EnableCharacterController();
        EnableMovement();
    }

    public void EnableCharacterController()
    {
        //print("Enabled Character Controller");
        playerControllerSprite.enabled = true;
        playerCutsceneSprite.enabled = false;
    }

    public void EnableMovement()
    {
        //print("Enabled Movement");
        playerGroundMover.AllowMovement();
        playerGroundMover.AllowTurning();
    }

}
