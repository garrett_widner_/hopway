﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class StatusTracker : MonoBehaviour
{
    public enum Type
    {
        Health,
        Mana,
        Stamina,
        Shield,
        Other
    };

    [Tooltip("Only useful to differentiate, has no effect on functionality")]
    public Type statusType;

    protected float maxStatLevel;
    protected float currentStatLevel;

    [Space(10)]
    public float startingStatLevel = 100;
    public float startingMaxStatLevel = 100;

    public float lowestMaxStatLevel = 20;
    public float highestMaxStatLevel = 500;

    public bool timedHealingStopsWhenReachingExtremes = true;

    private bool maxStatLevelHasBeenTriggered = false;

    public bool IsFull
    {
        get
        {
            return currentStatLevel > (maxStatLevel - .1f);
        }
    }

    /*
    public float TESTERVALUE = 40;

    public float INCREASETESTVALUE = 30;
    public float INCREASETESTSECONDS = 20;
    public float DECREASETESTVALUE = 50;
    public float DECREASETESTSECONDS = 20;
    */

    private List<StatLevelModifier> statLevelModifiers = new List<StatLevelModifier>();

    public delegate void StatusBarAction();
    public event StatusBarAction OnStatLevelHitZero;
    public event StatusBarAction OnStatLevelHitMax;

    public delegate void StatusBarAmountAction(float amount);
    public event StatusBarAmountAction OnStatLevelIncreasedImmediate;
    public event StatusBarAmountAction OnStatLevelDecreasedImmediate;
    public event StatusBarAmountAction OnStatLevelStartedTimedIncrease;
    public event StatusBarAmountAction OnStatLevelStartedTimedDecrease;
    public event StatusBarAmountAction OnStatLevelHadTimedDecreaseThisFrame;
    public event StatusBarAmountAction OnStatLevelHadTimedIncreaseThisFrame;

    //TODO: Hook these up
    public event StatusBarAmountAction OnMaxStatLevelStartedTimedIncrease;
    public event StatusBarAmountAction OnMaxStatLevelStartedTimedDecrease;
    public event StatusBarAmountAction OnMaxStatLevelIncreasedImmediate;
    public event StatusBarAmountAction OnMaxStatLevelDecreasedImmediate;
    public event StatusBarAmountAction OnMaxStatLevelHadTimedDecreaseThisFrame;
    public event StatusBarAmountAction OnMaxStatLevelHadTimedIncreaseThisFrame;

    public float CurrentStatLevel
    {
        get
        {
            return currentStatLevel;
        }
    }
    public float MaxStatLevel
    {
        get
        {
            return maxStatLevel;
        }
    }

    protected virtual void Start()
    {
        currentStatLevel = startingStatLevel;
        maxStatLevel = startingMaxStatLevel;
    }

    protected virtual void Update()
    {
        HandleTimedStatChanges();

        KeepCurrentStatLevelUnderMax();
        CheckForStatLevelHittingZero();
        CheckForStatLevelHittingMax();
    }

    protected void HandleTimedStatChanges()
    {
        float timedStatChangeThisFrame = 0;
        float timedMaxStatChangeThisFrame = 0;

        //Run each modifier
        foreach (StatLevelModifier modifier in statLevelModifiers)
        {
            if (modifier.AppliesToMaxLevel)
            {
                modifier.ChangeStatLevel(ref maxStatLevel, Time.deltaTime);
                modifier.ChangeStatLevel(ref timedMaxStatChangeThisFrame, Time.deltaTime);
            }
            else
            {
                modifier.ChangeStatLevel(ref currentStatLevel, Time.deltaTime);
                modifier.ChangeStatLevel(ref timedStatChangeThisFrame, Time.deltaTime);
            }
        }

        KeepMaxStatLevelInAppropriateRange();

        HandleFramewiseEvents(timedStatChangeThisFrame, timedMaxStatChangeThisFrame);
    }

    protected void HandleFramewiseEvents(float change, float maxChange)
    {
        if (change < 0)
        {
            if (OnStatLevelHadTimedDecreaseThisFrame != null)
            {
                OnStatLevelHadTimedDecreaseThisFrame(Mathf.Abs(change));
            }
        }
        else if (change > 0)
        {
            if (OnStatLevelHadTimedIncreaseThisFrame != null)
            {
                OnStatLevelHadTimedIncreaseThisFrame(Mathf.Abs(change));
            }
        }

        if (maxChange < 0)
        {
            if (OnMaxStatLevelHadTimedDecreaseThisFrame != null)
            {
                OnMaxStatLevelHadTimedDecreaseThisFrame(Mathf.Abs(maxChange));
            }
        }
        else if (maxChange > 0)
        {
            if (OnMaxStatLevelHadTimedIncreaseThisFrame != null)
            {
                OnMaxStatLevelHadTimedIncreaseThisFrame(Mathf.Abs(maxChange));
            }
        }
    }

    protected void KeepMaxStatLevelInAppropriateRange()
    {
        if (maxStatLevel < lowestMaxStatLevel)
        {
            maxStatLevel = lowestMaxStatLevel;
        }
        else if (maxStatLevel > highestMaxStatLevel)
        {
            maxStatLevel = highestMaxStatLevel;
        }
    }

    protected void KeepCurrentStatLevelUnderMax()
    {
        if (maxStatLevel < currentStatLevel)
        {
            currentStatLevel = maxStatLevel;
        }
    }

    protected void CheckForStatLevelHittingZero()
    {
        if (CurrentStatLevel <= 0)
        {
            currentStatLevel = 0;
            if (OnStatLevelHitZero != null)
            {
                OnStatLevelHitZero();
            }

            if (timedHealingStopsWhenReachingExtremes)
            {
                RemoveAllModifiersOfType(false);
            }
        }
    }

    protected void CheckForStatLevelHittingMax()
    {
        if (currentStatLevel >= maxStatLevel - .5f)
        {
            if(!maxStatLevelHasBeenTriggered)
            {
                maxStatLevelHasBeenTriggered = true;

                currentStatLevel = maxStatLevel;

                if (OnStatLevelHitMax != null)
                {
                    OnStatLevelHitMax();
                }
            }
            if (timedHealingStopsWhenReachingExtremes)
            {
                RemoveAllModifiersOfType(true);
            }
        }
    }


    protected void RemoveAllModifiersOfType(bool typeIsPositive)
    {
        List<StatLevelModifier> modifiersCopy = new List<StatLevelModifier>(statLevelModifiers);
        foreach (StatLevelModifier modifier in modifiersCopy)
        {
            if (modifier.IsPositive == typeIsPositive)
            {
                statLevelModifiers.Remove(modifier);
            }
        }
    }

    /*
    public void RunTests()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            IncreaseStatLevelImmediate(TESTERVALUE);
            print("Stat level increased by " + TESTERVALUE);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            DecreaseStatLevelImmediate(TESTERVALUE);
            print("Stat level decreased by " + TESTERVALUE);
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            IncreaseMaxStatLevel(TESTERVALUE, true);
            print("Max stat level increased by " + TESTERVALUE);
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            print(currentStatLevel + " / " + maxStatLevel);
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            SetCurrentStatLevelImmediate(TESTERVALUE);
            print("Stat level set to " + TESTERVALUE);
        }
    }

    public void RunModifierTests()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            IncreaseStatLevelOverTime(INCREASETESTVALUE, INCREASETESTSECONDS);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            DecreaseStatLevelOverTime(DECREASETESTVALUE, DECREASETESTSECONDS);
        }
    }
    */

    protected void DecreaseStatLevelImmediate(float decrease)
    {
        if (currentStatLevel > 0)
        {
            if (OnStatLevelDecreasedImmediate != null)
            {
                OnStatLevelDecreasedImmediate(decrease);
            }
            currentStatLevel -= decrease;
        }

        maxStatLevelHasBeenTriggered = false;
    }

    protected void IncreaseStatLevelImmediate(float increase)
    {
        if (currentStatLevel < maxStatLevel)
        {
            if (OnStatLevelIncreasedImmediate != null)
            {
                OnStatLevelIncreasedImmediate(increase);
            }
            currentStatLevel += increase;
        }
    }

    protected void IncreaseStatLevelOverTime(float increase, float seconds)
    {
        StatLevelModifier modifier = new StatLevelModifier(increase, seconds, false);
        statLevelModifiers.Add(modifier);
        StartCoroutine(RemoveStatLevelModifier(modifier, seconds));
        if (OnStatLevelStartedTimedIncrease != null)
        {
            OnStatLevelStartedTimedIncrease(increase);
        }
    }

    protected void DecreaseStatLevelOverTime(float decrease, float seconds)
    {
        StatLevelModifier modifier = new StatLevelModifier(-decrease, seconds, false);
        statLevelModifiers.Add(modifier);
        StartCoroutine(RemoveStatLevelModifier(modifier, seconds));
        if (OnStatLevelStartedTimedDecrease != null)
        {
            OnStatLevelStartedTimedDecrease(decrease);
        }

        maxStatLevelHasBeenTriggered = false;
    }

    protected IEnumerator RemoveStatLevelModifier(StatLevelModifier modifier, float afterSeconds)
    {
        yield return new WaitForSeconds(afterSeconds);
        statLevelModifiers.Remove(modifier);
    }

    /// <summary>
    /// Should only be used to change level to a set number. No events will be triggered acknowledging a decrease or increase
    /// in stat level, but if the stat level is brought to maximum or minimum, corresponding events will play.
    /// </summary>
    /// <param name="setLevel"></param>
    public virtual void SetCurrentStatLevelImmediate(float setLevel)
    {
        if (setLevel < maxStatLevel && setLevel > 0)
        {
            currentStatLevel = setLevel;
        }
        else if (setLevel <= 0)
        {
            currentStatLevel = 0;
            if (OnStatLevelHitZero != null)
            {
                OnStatLevelHitZero();
            }
        }
        else if (setLevel >= maxStatLevel)
        {
            currentStatLevel = maxStatLevel;
            if (OnStatLevelHitMax != null)
            {
                OnStatLevelHitMax();
            }
        }
    }

    protected void IncreaseMaxStatLevelImmediate(float increase, bool increaseCurrentHealthBySameAmount)
    {
        maxStatLevel += increase;
        if (OnMaxStatLevelIncreasedImmediate != null)
        {
            OnMaxStatLevelIncreasedImmediate(increase);
        }
        if (increaseCurrentHealthBySameAmount)
        {
            IncreaseStatLevelImmediate(increase);
        }
    }

    protected void IncreaseMaxStatLevelOverTime(float increase, float seconds, bool increaseCurrentHealthBySameAmount)
    {
        StatLevelModifier maxModifier = new StatLevelModifier(increase, seconds, true);
        statLevelModifiers.Add(maxModifier);
        StartCoroutine(RemoveStatLevelModifier(maxModifier, seconds));
        if (OnMaxStatLevelStartedTimedIncrease != null)
        {
            OnMaxStatLevelStartedTimedIncrease(increase);
        }

        if (increaseCurrentHealthBySameAmount)
        {
            IncreaseStatLevelOverTime(increase, seconds);
        }
    }

    protected void DecreaseMaxStatLevelImmediate(float decrease)
    {
        maxStatLevel -= decrease;
        if (OnMaxStatLevelDecreasedImmediate != null)
        {
            OnMaxStatLevelDecreasedImmediate(decrease);
        }
    }

    protected void DecreaseMaxStatLevelOverTime(float decrease, float seconds)
    {
        StatLevelModifier maxModifier = new StatLevelModifier(-decrease, seconds, true);
        statLevelModifiers.Add(maxModifier);
        StartCoroutine(RemoveStatLevelModifier(maxModifier, seconds));
        if (OnMaxStatLevelStartedTimedDecrease != null)
        {
            OnMaxStatLevelStartedTimedDecrease(decrease);
        }
    }

    public class StatLevelModifier
    {
        private float changePerSecond;
        private bool appliesToMaxLevel;

        public bool IsPositive
        {
            get
            {
                return (changePerSecond > 0) ? true : false;
            }
        }
        public bool AppliesToMaxLevel
        {
            get
            {
                return appliesToMaxLevel;
            }
        }

        public StatLevelModifier(float totalStatChange, float seconds, bool changeMaxLevel)
        {
            changePerSecond = totalStatChange / seconds;
            appliesToMaxLevel = changeMaxLevel;
        }

        public void ChangeStatLevel(ref float statLevelToChange, float currentFrameDuration)
        {
            statLevelToChange += currentFrameDuration * changePerSecond;
        }
    }

}
