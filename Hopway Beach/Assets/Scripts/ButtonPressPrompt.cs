﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ButtonPressPrompt : MonoBehaviour
{
    public Image promptImage;
    public float fadeTime = 0.8f;
    public float fadeInterludeTime = 0.5f;

    protected bool promptHasPlayed = false;
    protected bool promptIsPlaying = false;
    protected bool animationIsPlaying = false;
    protected bool shouldStopAnimation = false;

    protected virtual void Start()
    {
        promptImage.CrossFadeAlpha(0, 0, true);
    }

    protected virtual void Update()
    {
        if (!promptHasPlayed)
        {
            CheckForPromptStart();
        }

        if (promptIsPlaying)
        {
            if (!animationIsPlaying && !shouldStopAnimation)
            {
                animationIsPlaying = true;
                FadeInCycle();
            }

            CheckForPromptEnd();

            if(animationIsPlaying)
            {
                if(!shouldStopAnimation)
                {
                    CheckForAnimationPause();
                }
            }
            else
            {
                CheckForAnimationResume();
            }

            ApplyAnimationStoppage();
        }
    }

    protected virtual void FadeInCycle()
    {
        promptImage.CrossFadeAlpha(1, fadeTime, false);
        CancelInvoke("FadeOutCycle");
        Invoke("FadeOutCycle", fadeTime + fadeInterludeTime);
    }

    protected virtual void FadeOutCycle()
    {
        promptImage.CrossFadeAlpha(0, fadeTime, false);
        CancelInvoke("FadeInCycle");
        Invoke("FadeInCycle", fadeTime);
    }

    /// <summary>
    /// Should call StartPrompt if conditions are met
    /// </summary>
    protected abstract void CheckForPromptStart();

    protected virtual void StartPrompt()
    {
        promptIsPlaying = true;
    }

    /// <summary>
    /// Should call EndPrompt if specified conditions are met
    /// </summary>
    protected abstract void CheckForPromptEnd();

    protected virtual void EndPrompt()
    {
        //print(gameObject.name);
        promptIsPlaying = false;
        promptHasPlayed = true;
        shouldStopAnimation = true;
        CancelInvoke("FadeOutCycle");
        CancelInvoke("FadeInCycle");

    }


    /// <summary>
    /// Sets shouldStopAnimation to true or false based on specified conditions
    /// </summary>
    protected abstract void CheckForAnimationPause();
    protected void PauseAnimation()
    {
        shouldStopAnimation = true;
    }
    protected abstract void CheckForAnimationResume();
    protected void ResumeAnimation()
    {
        shouldStopAnimation = false;
    }

    protected virtual void ApplyAnimationStoppage()
    {
        if (shouldStopAnimation && animationIsPlaying)
        {
            animationIsPlaying = false;
            CancelInvoke("FadeInCycle");
            CancelInvoke("FadeOutCycle");
            promptImage.CrossFadeAlpha(0, fadeTime / 2, false);
            //print("Supposedly stopping animation for " + gameObject.name);
        }
        else if(!shouldStopAnimation && !animationIsPlaying)
        {
            //print("animation got stuck for " + gameObject.name);
            animationIsPlaying = true;
            FadeInCycle();
        }
    }
}
