﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleSoundPlayer : MonoBehaviour
{
    public AudioSource soundSource;

    public float maxRandomPitch = 1.2f;
    public float minRandomPitch = 0.8f;

    protected void PlaySound()
    {
        soundSource.Play();
    }

    protected void PlaySoundWithRandomPitch()
    {
        soundSource.pitch = Random.Range(minRandomPitch, maxRandomPitch);
        soundSource.Play();
        CancelInvoke();
        Invoke("ResetPitch", 0.2f);
    }

    protected void ResetPitch()
    {
        soundSource.pitch = 1f;

    }
}
