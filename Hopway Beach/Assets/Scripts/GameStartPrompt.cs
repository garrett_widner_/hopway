﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStartPrompt : ButtonPressPrompt
{

    public CutsceneSequencer cutsceneSequencer;

    private void OnEnable()
    {
        cutsceneSequencer.OnCutsceneStart += EndPromptOnCutsceneEnd;
        cutsceneSequencer.OnCutsceneEnd += EndPromptOnCutsceneEnd;
    }

    private void OnDisable()
    {
        cutsceneSequencer.OnCutsceneStart -= EndPromptOnCutsceneEnd;
        cutsceneSequencer.OnCutsceneEnd -= EndPromptOnCutsceneEnd;
    }

    protected override void Start()
    {
        base.Start();
        StartPrompt();
    }

    public void EndPromptOnCutsceneEnd()
    {
        EndPrompt();
        promptImage.CrossFadeAlpha(0, 0, true);
    }

    protected override void CheckForAnimationPause()
    {
    }

    protected override void CheckForPromptEnd()
    {
    }

    protected override void CheckForAnimationResume()
    {
    }

    protected override void CheckForPromptStart()
    {
    }

}
