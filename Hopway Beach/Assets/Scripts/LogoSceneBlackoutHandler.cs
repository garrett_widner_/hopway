﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoSceneBlackoutHandler : MonoBehaviour
{
    public ScreenBlackout blackoutScript;
    public float logoShowTime = 2f;
    private bool logoHasBeenShown = false;

    public delegate void BlackoutAction();
    public BlackoutAction OnLogoSequenceFinished;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += StartFadeInSequence;
        blackoutScript.OnScreenCompletelyVisible += StartTimedBlackout;
        blackoutScript.OnScreenCompletelyBlack += TriggerLogoSequenceFinishedIfLogoShown;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= StartFadeInSequence;
        blackoutScript.OnScreenCompletelyVisible -= StartTimedBlackout;
        blackoutScript.OnScreenCompletelyBlack -= TriggerLogoSequenceFinishedIfLogoShown;

    }

    private void StartFadeInSequence(Scene s, LoadSceneMode ls)
    {
        blackoutScript.FadeIn();
    }

    private void StartTimedBlackout()
    {
        Invoke("StartBlackoutSequence", logoShowTime);
        logoHasBeenShown = true;
    }

    private void StartBlackoutSequence()
    {
        blackoutScript.BlackOut();
    }

    private void TriggerLogoSequenceFinishedIfLogoShown()
    {
        if(logoHasBeenShown)
        {
            if(OnLogoSequenceFinished != null)
            {
                OnLogoSequenceFinished();
            }
        }
    }
}
