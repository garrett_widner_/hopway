﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodPileUISoundPlayer : SingleSoundPlayer
{
    public AudioClip dings;
    public AudioClip pop;

    public float timeToPlayAfterFoodIconChange = 1.5f;
    public float timeBetweenDingsAndPop = 2f;
    public int maxPopCount = 5;
    public int popIncrement = 2;
    public int popStart = 1;
    private int currentPops;
    public FoodPileUI foodPile;
    public PilePlacer pile;

    private bool waitIsOver = true;

    private void OnEnable()
    {
        foodPile.OnTimeForFoodPileUpgrade += ScheduleSoundPlays;
        pile.OnPilePlaced += PlayPops;
    }

    private void OnDisable()
    {
        foodPile.OnTimeForFoodPileUpgrade -= ScheduleSoundPlays;
        pile.OnPilePlaced -= PlayPops;
    }

    private void Start()
    {
        currentPops = popStart;
    }

    private void ScheduleSoundPlays()
    {
        Invoke("PlaySounds", timeToPlayAfterFoodIconChange);
    }

    private void PlaySounds()
    {
        soundSource.clip = dings;
        soundSource.Play();
        Invoke("PlayPops", timeBetweenDingsAndPop);
    }

    private void PlayPops()
    {
        StartCoroutine(PlayPopSequence());
    }

    private IEnumerator PlayPopSequence()
    {
        soundSource.clip = pop;
        PlaySoundWithRandomPitch();
        for (int i = 1; i <= currentPops; i++)
        {
            waitIsOver = false;
            float timeBetween = Random.Range(0.02f, 0.05f);
            Invoke("PlaySinglePop", i * 0.02f);
            while(!waitIsOver)
            {
                yield return null;
            }
        }
        currentPops += popIncrement;
    }

    private void PlaySinglePop()
    {
        PlaySoundWithRandomPitch();
        waitIsOver = true;
    }

    public void PlayPop()
    {
        soundSource.clip = pop;

        PlaySinglePop();
        //print("Played");
    }

}
