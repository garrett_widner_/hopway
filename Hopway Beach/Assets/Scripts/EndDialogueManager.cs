﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EndDialogueManager : MonoBehaviour
{
    public Text endTextBox;
    public Text endCaptionBox;

    public Animator textBoxAnimator;
    public Animator captionBoxAnimator;

    public Animator creditsAnimator;
    public Animator soundAttributionsAnimator;

    public string openAnimationTrigger;
    public string closeAnimationTrigger;

    public Statement[] endStatements;

    public void SetText(int ending)
    {
        if(ending <= endStatements.Length)
        {
            endTextBox.text = endStatements[ending - 1].text;
            endCaptionBox.text = endStatements[ending - 1].characterName;
        }
        else
        {
            Debug.LogWarning("Warning: Ending number greater than number of text options");
        }
    }

    public void OpenTextBox()
    {
        textBoxAnimator.SetTrigger(openAnimationTrigger);
    }

    public void CloseTextBox()
    {
        textBoxAnimator.SetTrigger(closeAnimationTrigger);
    }

    public void OpenCaptionBox()
    {
        captionBoxAnimator.SetTrigger(openAnimationTrigger);
    }

    public void CloseCaptionBox()
    {
        captionBoxAnimator.SetTrigger(closeAnimationTrigger);
    }
    
    public void OpenCredits()
    {
        creditsAnimator.SetTrigger(openAnimationTrigger);

    }

    public void CloseCredits()
    {
        creditsAnimator.SetTrigger(closeAnimationTrigger);
    }

    public void OpenSoundAttributions()
    {
        soundAttributionsAnimator.SetTrigger(openAnimationTrigger);
    }

    public void CloseSoundAttributions()
    {
        soundAttributionsAnimator.SetTrigger(closeAnimationTrigger);
    }




}
