﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabScurrySoundHandler : SingleSoundPlayer
{
    public Crab crab;

    private void OnEnable()
    {
        crab.OnScuttleStart += StartScuttle;
        crab.OnScuttleEnd += StopScuttle;
    }

    private void OnDisable()
    {
        crab.OnScuttleStart -= StartScuttle;
        crab.OnScuttleEnd -= StopScuttle;
    }

    private void StartScuttle()
    {
        PlaySound();
    }

    private void StopScuttle()
    {
        soundSource.Stop();
    }

}
