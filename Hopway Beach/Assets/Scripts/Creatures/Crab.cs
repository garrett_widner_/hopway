﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crab : GroundMover
{
    public float minWanderTime = 0.5f;
    public float maxWanderTime = 1.5f;
    public float minWaitTime = 1f;
    public float maxWaitTime = 2.3f;
    public CircleCollider2D threatRadius;
    public LayerMask threatLayer;

    public override float CurrentSpeed
    {
        get
        {
            if(isThreatened)
            {
                return runSpeed;
            }
            return baseSpeed;
        }
    }

    public bool IsThreatened
    {
        get
        {
            return isThreatened;
        }
    }

    private bool isRunningSequence = false;
    private bool isThreatened = false;
    private bool wasThreatenedLastFrame = false;
    private Vector2 threatLocation = Vector2.zero;

    private Vector2 input = Vector2.zero;

    public delegate void CrabAction();
    public CrabAction OnScuttleStart;
    public CrabAction OnScuttleEnd;

    private bool IsNewlyThreatened
    {
        get
        {
            return (isThreatened && !wasThreatenedLastFrame);
        }
    }

    private bool StoppedBeingThreatened
    {
        get
        {
            return (!isThreatened && wasThreatenedLastFrame);
        }
    }

    public override void Start()
    {
        base.Start();
        isSetup = true;
    }

    protected override void Update()
    {
        SetMovingStatus();
        velocityLastFrame = Vector2.zero;

        Vector2 velocity = Vector2.zero;


        CheckForThreat();

        RunScuttleEvents();

        if(IsNewlyThreatened || StoppedBeingThreatened)
        {
            StopAllSequences();
        }

        if (!isThreatened && !isRunningSequence)
        {
            StartCoroutine("Wander");
        }
        else if(isThreatened)
        {
            Scuttle();
        }

        input = input.ClosestCardinalOrOrdinalDirectionNormalized() * input.magnitude;

        if (CanTurn)
        {
            collisionInfo.SetFaceDirection(input);
        }

        Move(ref velocity, input);

        transform.Translate(velocity);
    }

    private void StopAllSequences()
    {
        isRunningSequence = false;
        StopAllCoroutines();
    }

    private IEnumerator Wander()
    {
        isRunningSequence = true;
        float waitTime = Random.Range(minWaitTime, maxWaitTime);
        yield return new WaitForSeconds(waitTime);

        float wanderTime = Random.Range(minWanderTime, maxWanderTime);
        Vector2 direction = new Vector2(Random.Range(-1, 2), Random.Range(-1, 2));

        //Make sure direction is randomly cardinalized, no intercardinals
        
        if (Mathf.Abs(direction.y) > 0.1f && Mathf.Abs(direction.x) > 0.1f)
        {
            int random = Random.Range(0, 2);
            switch(random)
            {
                case 0:
                    direction = new Vector2(direction.x, 0);
                    break;
                case 1:
                    direction = new Vector2(0, direction.y);
                    break;
                default:
                    break;
            }
        }
        
        input = direction;

        yield return new WaitForSeconds(wanderTime);
        input = Vector2.zero;
        isRunningSequence = false;
        yield return null;
    }

    private void Scuttle()
    {
        Vector2 heading = threatLocation - (Vector2)transform.position;
        float magnitude = heading.magnitude;
        Vector2 direction = heading / magnitude;

        if(float.IsNaN(direction.y))
        {
            direction = new Vector2(direction.x, 0);
        }
        if(float.IsNaN(direction.x))
        {
            direction = new Vector2(0, direction.y);
        }

        input = -direction;
    }

    private void CheckForThreat()
    {
        wasThreatenedLastFrame = isThreatened;

        Collider2D threatCollider = Physics2D.OverlapCircle(transform.position, threatRadius.radius * transform.localScale.x, threatLayer);
        if(threatCollider != null)
        {
            //SuperDebugger.DrawPlusAtPoint(threatLocation, Color.red, 0.6f, 1f);
            //print("Found a threat " + threatCollider.name + " at " + threatLocation);
            isThreatened = true;
            threatLocation = threatCollider.transform.position;
        }
        else
        {
            isThreatened = false;
            threatLocation = Vector2.zero;
        }
    }

    private void RunScuttleEvents()
    {
        if (wasThreatenedLastFrame && !isThreatened)
        {
            if (OnScuttleEnd != null)
            {
                OnScuttleEnd();
            }
        }
        else if (!wasThreatenedLastFrame && isThreatened)
        {
            if (OnScuttleStart != null)
            {
                OnScuttleStart();
            }
        }
    }

}
