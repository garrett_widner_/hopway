﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquidAnimationHandler : MonoBehaviour
{
    public HopMover hopMover;
    public UrchinDeathHandler squidDeathHandler;
    public Animator animator;
    public Urchin urchin;

    private int isAirborneHash = Animator.StringToHash("IsAirborne");
    private int isRelaxingHash = Animator.StringToHash("IsRelaxing");
    private int isDeadHash = Animator.StringToHash("IsDead");

    private void OnEnable()
    {
        hopMover.OnHopEnded += EndHop;
        hopMover.OnHopStarted += StartHop;
        squidDeathHandler.OnKilled += Die;
        urchin.OnBecameVulnerable += Relax;
        urchin.OnBecameInvulnerable += StopRelaxing;
    }

    private void OnDisable()
    {
        hopMover.OnHopEnded -= EndHop;
        hopMover.OnHopStarted -= StartHop;
        squidDeathHandler.OnKilled -= Die;
        urchin.OnBecameVulnerable -= Relax;
        urchin.OnBecameInvulnerable -= StopRelaxing;
    }

    private void StartHop()
    {
        animator.SetBool(isAirborneHash, true);
    }

    private void EndHop()
    {
        animator.SetBool(isAirborneHash, false);
    }

    private void Die()
    {
        animator.SetBool(isDeadHash, true);
    }

    private void Relax()
    {
        animator.SetBool(isRelaxingHash, true);
    }

    private void StopRelaxing()
    {
        animator.SetBool(isRelaxingHash, false);
    }

}
