﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabAnimationHandler : MonoBehaviour
{
    public GroundMover groundMover;
    public Crab crab;
    public Animator animator;

    private int isScuttlingHash = Animator.StringToHash("IsScuttling");
    private int isMovingHash = Animator.StringToHash("IsMoving");

    private void Update()
    {
        if (groundMover.IsMoving != animator.GetBool(isMovingHash))
        {
            animator.SetBool(isMovingHash, groundMover.IsMoving);
        }

        if(crab.IsThreatened != animator.GetBool(isScuttlingHash))
        {
            animator.SetBool(isScuttlingHash, crab.IsThreatened);
        }
    }

}
