﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquidSoundHandler : SingleSoundPlayer
{
    public UrchinDeathHandler deathHandler;
    public HopMover hopMover;

    public AudioClip squidPlop;
    public AudioClip squidDeath;

    public float deathVolume = .7f;
    public float plopVolume = 1.2f;


    private void OnEnable()
    {
        deathHandler.OnKilled += PlayDeathSound;
        hopMover.OnHopEnded += PlayPlopSound;
    }

    private void OnDisable()
    {
        deathHandler.OnKilled -= PlayDeathSound;
        hopMover.OnHopEnded -= PlayPlopSound;
    }

    public void PlayDeathSound()
    {
        soundSource.Stop();
        soundSource.volume = deathVolume;
        soundSource.clip = squidDeath;
        PlaySound();
    }

    public void PlayPlopSound()
    {
        if(!deathHandler.IsDying)
        {
            soundSource.Stop();
            soundSource.volume = plopVolume;
            soundSource.clip = squidPlop;
            PlaySoundWithRandomPitch();
        }
    }

}
