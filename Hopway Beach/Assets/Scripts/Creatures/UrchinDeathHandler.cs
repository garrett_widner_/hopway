﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UrchinDeathHandler : MonoBehaviour
{
    public LayerMask whatKills;
    public BoxCollider2D urchinCollider;
    public Urchin urchin;
    public GameObject meatPrefab;
    public HopHandler hopHandler;
    public HopMover hopMover;

    private bool isDying = false;
    public bool IsDying
    {
        get
        {
            return isDying;
        }
    }

    public delegate void DeathAction();
    public event DeathAction OnKilled;

    public void OnEnable()
    {
        hopMover.OnHopHalfOver += LookLikeMeat;
        hopMover.OnHopEnded += BecomeMeat;
    }

    public void OnDisable()
    {
        hopMover.OnHopHalfOver -= LookLikeMeat;
        hopMover.OnHopEnded -= BecomeMeat;
    }

    private void Update()
    {
        if(!isDying)
        {
            if (urchin.IsVulnerable)
            {
                Collider2D foundCollider = Physics2D.OverlapArea(urchinCollider.bounds.max, urchinCollider.bounds.min, whatKills);
                if (foundCollider != null)
                {
                    if(OnKilled != null)
                    {
                        OnKilled();
                    }
                    isDying = true;
                    urchin.EndHopSequence();
                    hopHandler.HopInRandomDirection(true);

                }
            }
        }
        
    }

    private void LookLikeMeat()
    {
        if(isDying)
        {
            /// tell animator to display meat poof animation
        }
    }

    private void BecomeMeat()
    {
        if(isDying)
        {
            Instantiate(meatPrefab, transform.position, Quaternion.identity);
            Destroy(this.gameObject, 0.1f);
        }
    }



}
