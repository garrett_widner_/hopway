﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TextPromptImageChanger : MonoBehaviour
{
    public Image promptImage;
    [Space]
    public Sprite controllerPress;
    public Sprite keyboardPress;

    private PlayerActions playerActions;

    private bool isCurrentlyUsingController = false;

    private void Start()
    {
        playerActions = PlayerActions.CreateWithDefaultBindings();
        CheckForInputTypeChangeAndUpdateImage();
    }

    private void CheckForInputTypeChangeAndUpdateImage()
    {
        bool wasUsingController = isCurrentlyUsingController;
        isCurrentlyUsingController = (playerActions.LastInputType == InControl.BindingSourceType.DeviceBindingSource);

        if (isCurrentlyUsingController != wasUsingController)
        {
            UpdatePromptImage();
        }
    }

    private void UpdatePromptImage()
    {
        if (isCurrentlyUsingController)
        {
            promptImage.sprite = controllerPress;
        }
        else if (!isCurrentlyUsingController)
        {
            promptImage.sprite = keyboardPress;
        }
    }

    private void Update()
    {
        CheckForInputTypeChangeAndUpdateImage();
    }
}
