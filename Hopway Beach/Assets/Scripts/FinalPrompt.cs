﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalPrompt : ButtonPressPrompt
{
    protected override void CheckForAnimationPause()
    {
    }

    protected override void CheckForAnimationResume()
    {
    }

    protected override void CheckForPromptEnd()
    {
    }

    protected override void CheckForPromptStart()
    {
    }

    public void ShowPrompt()
    {
        if(!promptIsPlaying)
        {
            StartPrompt();
        }
        else if(!animationIsPlaying)
        {
            ResumeAnimation();
        }
    }

    public void HidePrompt()
    {
        if(animationIsPlaying)
        {
            PauseAnimation();
        }
    }
}
