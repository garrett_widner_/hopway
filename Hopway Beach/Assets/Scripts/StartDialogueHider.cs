﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class StartDialogueHider : MonoBehaviour
{
    public CutsceneSequencer startCutscene;
    public string startSceneName;

    public PauseManager pauseManager;

    public EndingsUIDisplayer endingsUIDisplayer;
    public GameObject resolutionUI;
    public GameObject quitUI;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += CheckForShowDialogue;
        startCutscene.OnCutsceneStart += HideDialogue;
        pauseManager.OnPaused += ShowDialogue;
        pauseManager.OnUnpause += HideDialogue;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= CheckForShowDialogue;
        startCutscene.OnCutsceneStart -= HideDialogue;
        pauseManager.OnPaused -= ShowDialogue;
        pauseManager.OnUnpause -= HideDialogue;
    }

    private void HideDialogue()
    {
        //startDialogue.SetActive(false);
        resolutionUI.SetActive(false);
        quitUI.SetActive(false);
        endingsUIDisplayer.HideAll();
    }

    private void CheckForShowDialogue(Scene scene, LoadSceneMode lsm)
    {
        if (scene.name == startSceneName)
        {
            ShowDialogue();
        }
    }

    private void ShowDialogue()
    {
        //print("Check whether to show endings commenced");
        //startDialogue.SetActive(true);
        resolutionUI.SetActive(true);
        quitUI.SetActive(true);

        if(EndingSaver.Instance != null)
        {
            if (EndingSaver.Instance.AreAnyEndingsUnlocked)
            {
                //print("Evidently, at least one ending is unlocked");
                endingsUIDisplayer.ShowAll();
            }
            else
            {
                //print("All endings are locked");
                endingsUIDisplayer.HideAll();
            }
        }

    }
}
