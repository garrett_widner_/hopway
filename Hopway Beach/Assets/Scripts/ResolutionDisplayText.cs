﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ResolutionDisplayText : MonoBehaviour
{
    public Text text;

    private void Update()
    {
        text.text = "Current Resolution: " + Screen.width + " x " + Screen.height;
    }

}
